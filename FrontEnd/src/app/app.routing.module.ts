import { NgModule }              from '@angular/core';

import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './home/home.component';
import {CategoryComponent} from './pages/category.component/category.component'
import {AddCategoryComponent} from './pages/category.component/addCategory.componemt';
import {ModifyCategoryComponent} from './pages/category.component/modifyCategory.compoment';

//Publisher
import {PublisherComponent} from './pages/publisher.componemt/index.compoment';
import {AddPublisherComponent} from './pages/publisher.componemt/addPublisher.componemt';
import {ModifyPublisherComponent} from './pages/publisher.componemt/modifyPublisher.compoment';


//Authen
import {LoginComponent} from './auth/login.component/login.component'
import {ProfileComponent} from './auth/profile.component/profile.component'
import {RegisterComponent} from './auth/register.component/register.component'
import {UnAuthenGuard, AuthenGuard, AuthenSellerGuard, AuthenManagerGuard, AuthenAdminGuard} from './auth/guards/authen-guard'
//User
import {UserListComponent} from './pages/user.component/user-list.component/user-list.component'
import {EditUserComponent}  from './pages/user.component/edit-user.component/edit-user.component'
import {ProductListComponent} from './pages/product.component/product-list.component/product-list.component'

//Order
import {OrderComponent} from './pages/orderManager.componemt/index.component';
import {ProductCreateComponent}  from './pages/product.component/product-create.component/product-create.component'
import {SaleComponent} from './sale/sale.component/sale.component'

//Import 
import {ImportComponent} from './import_good/import_good.component/import_good.component'
//Cost
import {CostListComponent} from './pages/cost.component/cost-list.component/cost-list.component'
import {CostEditComponent} from './pages/cost.component/cost-edit.component/cost-edit.component'
import {CostAddComponent} from './pages/cost.component/cost-add.component/cost-add.component'
import {ImportManamentComponent} from './import_good/import-managment.component/import-managment.component'
import {ImportEditComponent} from './import_good/import-edit.component/import-edit.component'
import {ReportGeneralComponent} from './report/report-general.component/report-general.component'
import {DeliveryManagementComponent}  from './pages/delivery.component/delivery.management.component/delivery.management.component'
import {DeliveryEditComponent} from './pages/delivery.component/delivery-edit.component/delivery-edit.component'
import {ProductEditComponent} from './pages/product.component/product-edit.component/product-edit.component'

const appRoutes: Routes = [

  //Cần quyền đăng nhập
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', canActivate: [AuthenGuard], component: HomeComponent},
  { path: 'profile',  canActivate: [AuthenGuard], component: ProfileComponent},

  //Không cần quyền đăng nhập
  { path: 'login',  canActivate: [UnAuthenGuard], component: LoginComponent},

  //Quản lý sản phẩm -->>>> Quyền quản lý manager
  { path: 'products', canActivate: [AuthenManagerGuard], component: ProductListComponent },
  { path: 'products/create', canActivate: [AuthenManagerGuard], component: ProductCreateComponent },
  { path: 'products/:productID', canActivate: [AuthenManagerGuard], component: ProductEditComponent },

  //Quản lý người dùng nhân sự -->>>> Quyền admin 
  { path: 'users', canActivate: [AuthenAdminGuard], component: UserListComponent },
  { path: 'users/create',  component: RegisterComponent},
  { path: 'users/:userID', canActivate: [AuthenAdminGuard], component: EditUserComponent},

  //Quản lý thể loại -->>>> Quyền admin 
  { path: 'categories', canActivate: [AuthenAdminGuard], component: CategoryComponent },
  { path: 'categories/create', canActivate: [AuthenAdminGuard], component:  AddCategoryComponent},  
  { path: 'categories/:id', canActivate: [AuthenAdminGuard], component:  ModifyCategoryComponent},
  //Quản lý nhà cung cấp -->>>> Quyền admin 
  { path: 'publishers', canActivate: [AuthenAdminGuard], component:  PublisherComponent},
  { path: 'publishers/create', canActivate: [AuthenAdminGuard], component:  AddPublisherComponent},
  { path: 'publishers/:id', canActivate: [AuthenAdminGuard], component:  ModifyPublisherComponent},

  //Bán hàng & Quản lý đơn hàng -->>>> Quyền bán hàng seller
  { path: 'sale',  canActivate: [AuthenSellerGuard],  component: SaleComponent},
  { path: 'orders',  canActivate: [AuthenSellerGuard], component:  OrderComponent},
  //Nhập hàng & Quản lý đơn nhập -->>>> Quyền manager
  { path: 'imports', canActivate: [AuthenManagerGuard],  component: ImportManamentComponent},
  { path: 'imports/:importID', canActivate: [AuthenManagerGuard], component: ImportEditComponent},
  { path: 'import', canActivate: [AuthenManagerGuard],  component: ImportComponent},

  //Quản lý phí vẩn chuyển -->>>>  Quyền admin
  { path: 'cost', canActivate: [AuthenAdminGuard], component: CostListComponent},
  { path: 'cost/create', canActivate: [AuthenAdminGuard], component: CostAddComponent},
  { path: 'cost/:costID',canActivate: [AuthenAdminGuard], component: CostEditComponent},

  //Báo cáo ->>>> Quyền manager
  { path: 'report', canActivate: [AuthenManagerGuard], component: ReportGeneralComponent},

  //Quản lý nơi giao hàng -->>>> Quyền bán hàng
  { path: 'delivery', canActivate: [AuthenSellerGuard], component: DeliveryManagementComponent},
  { path: 'delivery/:deliveryID', canActivate: [AuthenSellerGuard], component: DeliveryEditComponent}


];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}
