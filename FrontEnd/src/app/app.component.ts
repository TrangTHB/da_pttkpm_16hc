import { Component } from '@angular/core';
declare var Layout: any
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
   ngAfterViewInit(): void{
        document.addEventListener("DOMContentLoaded", function(event) {
            Layout.init();
        });
    }
}
