import {Injectable} from '@angular/core'
import {IUser} from '../model/user'
import {IRole} from '../model/role'
import {Response, Headers} from '@angular/http'
import {HttpInterceptor} from '../http.interceptor'
import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/catch'
import {AuthService} from '../auth/auth.service'
import {AppConfig} from '../app.config'
import {IDeleteUserModel} from '../pages/user.component/edit-user.component/editusermodel'

@Injectable()
export class UserService{
    private _hostApi: string;
    public user: any = {name: '', img: '', role: ''};

    constructor(private _http: HttpInterceptor, private authService: AuthService){
        this._hostApi = AppConfig.hostapi;
        if (this.authService.auth.isAuth == true){
           this.getNewInfo();
        }
    }
  
    public getNewInfo(){
         this.getInfo().subscribe(user => {
                this.user.name = user.name;
                this.user.img = user.img;
                this.user.role = user.role.name;
            }, err => {
                console.log(err);
            })
    }
    public clearUserInfo(){
        this.user.name = '';
        this.user.img = '';
        this.user.role = '';
    }
    getInfo(): Observable<IUser>{
       
        return this._http.get(this._hostApi + '/api/user/getinfo')
            .map((response: Response) => <IUser> response.json())
            .catch(this.handleError);
    }
    getList(): Observable <IUser[]>{
        return this._http.get(this._hostApi + '/api/user')
            .map((response: Response) => <IUser[]> response.json())
            .catch(this.handleError);
    }
    deleteUser(userId: string): Observable<void>{
        return this._http.delete(`${this._hostApi}/api/user/${userId}`)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }
    getRoles(): Observable<IRole[]>{
        return this._http.get(this._hostApi + '/api/user/getroles')
            .map((response: Response) => <IRole[]> response.json())
            .catch(this.handleError);
    }
    getInfoByUserId(userID: string){
        return this._http.get(this._hostApi + '/api/user/' + userID)
            .map((response: Response) => <IUser> response.json())
            .catch(this.handleError);
    }
    updateUser(model: IDeleteUserModel){
        var formData = new FormData();
        if (model.img != null)
        {
            formData.append('img', model.img);
        }
        formData.append('model', JSON.stringify(model));
        var headers = new Headers();
        headers.append('enctype', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        model.img = null;
        return this._http.put(`${this._hostApi}/api/user/${model._id}`, formData, {headers: headers})
        .map((response: Response) => response.json())
        .catch(this.handleError);
        
    }
  
    private handleError(error: Response)
    {
        return Observable.throw(error.json() || 'Server error');
    }
}