import { Component, OnInit} from '@angular/core';

import {ReportService} from '../../services/report.service'
import {NotificationService} from '../../services/notification.service'
declare var jQuery:any;
declare var Chart: any
import {IYearReport} from '../../model/IReport'

interface Model{
  year: number,
  month_begin: number,
  month_end: number
}

@Component({
  templateUrl: './report-general.component.html',
  styleUrls: ['./report-general.component.css'],
  providers: [NotificationService],
  moduleId: module.id
})

export class ReportGeneralComponent implements OnInit  {
  
    public isCreating: boolean = false;
    public model: Model
    public result: IYearReport
    constructor(private _reportService: ReportService, private _notiService: NotificationService){
      this.model= {
        year: new Date().getFullYear(),
        month_begin: 1,
        month_end: 12
      };
    
    };
     
    ngOnInit(): void {
        this.getReport();
    }
    getReport(){
      this.isCreating = true;
      this._reportService.getReport(this.model.year, this.model.month_begin, this.model.month_end).subscribe(h => {
        this.result = h[0];
        this.isCreating = false;
        let lineChartData = [];
        
            let element1: any = {};
            element1.data = this.result.months.map((h) => h.totalMoneyOrder);
            element1.label= `Tổng tiền bán`
            element1.backgroundColor= "rgba(51, 153, 64, 0)";
						element1.borderColor= "rgba(51, 153, 64, 1)";
             let element2: any = {};
            element2.data = this.result.months.map((h) => h.totalMoneyImportGood);
            element2.label= `Tổng tiền chi`
            element2.backgroundColor= "rgba(153, 51, 139, 0)";
						element2.borderColor= "rgba(153, 51, 139, 1)";
            let element3: any = {};
            element3.data = this.result.months.map((h) => h.profit);
            element3.label= `Lãi`
            element3.backgroundColor= "rgba(206, 0, 0, 0.2)";
						element3.borderColor= "rgba(206, 0, 0, 1)";
        lineChartData.push(element1);
        lineChartData.push(element2);
        lineChartData.push(element3);
         var ctx = document.getElementById("myChart");
         var myChart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: this.result.months.map((h) => `Tháng ${h.month}`),
              datasets: lineChartData
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero:true
                      }
                  }]
              }
          }
      });
      
      }, error => {
        this._notiService.printErrorMessage("Có lỗi trong quá trình xử lý");
        console.log(error);
      });
    }
    
   
   
}
