export interface YearReport {

}
export interface MonthReport{
    totalAmountOrder: number,
    totalMoneyOrder: number,
    totalAmountImportGood: number,
    totalMoneyImportGood: number,
    profit: number
}