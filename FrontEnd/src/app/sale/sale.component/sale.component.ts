import {Component} from '@angular/core'
import {ProductService} from '../../services/product.service'
import {IProduct} from '../../model/IProduct'
import {IOrder, IOrderDetail} from '../salemodel'
import {ICost} from '../../model/ICost'
import {NotificationService} from '../../services/notification.service'
import {CostService} from '../../services/cost.service'
import {OrderService} from '../../services/service_order'
@Component({
    templateUrl: './sale.component.html',
    styleUrls: ['./sale.component.css']
})

export class SaleComponent {
    public products: IProduct[];
    public keyword: string;
    public Order: IOrder;
    public costs: ICost[] = [];
    public isCreating: boolean = false;
    public messages: string[] = [];

    constructor(private _productService: ProductService,
                private _notiService: NotificationService,
                private _costService: CostService,
                private _OrderService: OrderService){
        this.setInitOrder();
       
        this._costService.getList().subscribe(cost => this.costs = cost, error => {
            console.log(error);
        })
        this.loadProducts();
    }
    loadProducts(){
        this._productService.getListForSale().subscribe(h => {
            this.products = h;
        }, error => {
            console.log(error);
        })
    }
    updateDate(){
        let date = new Date(this.Order.delivery.delivery_date);
        
        if (isNaN(date.getTime()))
        {
            this.Order.delivery.delivery_date = new Date();
        };
    }
    setInitOrder(){
        this.Order = {
            orderDetail: [],
            totalMoney: 0,
            totalAmount: 0,
            statement: '-1',
            typePayment: 1,
            cost: '-1',
            delivery: {
                _id: '',
                address: '',
                cost: 0,
                customer_name: '',
                delivery_date: new Date(),
                note: '',
                phone: '',

            }
        }
        this.calcTotalMoney();
        this.products = undefined;
    }
    addProduct(product: IProduct){
        
        let orderDetail: IOrderDetail = this.Order.orderDetail.find(h => h.product._id == product._id);
        if (orderDetail){
            if (orderDetail.product.qty > orderDetail.amount){
                orderDetail.amount++;
                this.calcTotalMoney();
            }
            else{
                this._notiService.printSuccessMessage("Sản phẩm tồn không đủ !");
            }
        }
        else{
            orderDetail = {
                amount: 1,
                price: product.promotion_price,
                product: product
            };
            this.Order.orderDetail.push(orderDetail);
            this.calcTotalMoney();

        }
    }
    calcTotalMoney(){
        this.Order.totalMoney = 0;
        this.Order.totalAmount = 0;
        this.Order.orderDetail.forEach(h => {
            this.Order.totalMoney += h.price * h.amount;
            this.Order.totalAmount += h.amount;
        })
        if (this.Order.typePayment == 2){
            let cost = this.costs.find(h => h._id == this.Order.cost);
            if (cost){
                this.Order.totalMoney += cost.price;
            }
        }
    }
    remove(detail: IOrderDetail){
        let index = this.Order.orderDetail.findIndex(h => h.product._id == detail.product._id);
        this.Order.orderDetail.splice(index, 1);
         this.calcTotalMoney();
    }
    onChangeArea(){

        let cost = this.costs.find(h => h._id == this.Order.cost);
        if (cost){
                this.Order.delivery.cost = cost.price;
        }
        this.calcTotalMoney();
    }
    onSubmit(){
        this.isCreating = true;
        console.log(this.Order);
        this._OrderService.createOrder(this.Order).subscribe(h => {
            console.log(h);
            this.setInitOrder();
            this.loadProducts();
            this._notiService.printSuccessMessage("Thêm đơn hàng nhập thành công");
            this.isCreating = false;
        }, error => {
            this.messages = error.lstErr;
            this.isCreating = false;
            console.log(error);
        })
    }
    editQty(detail: IOrderDetail){
        if (detail.amount == null  || Number.isInteger(detail.amount) == false){
            detail.amount = 1;
            this._notiService.printSuccessMessage("Số lượng sản phẩm không đủ hoặc không đúng định dạng !");
        }
        else
        {
            if (detail.amount < 1){
                detail.amount = 1;
                this._notiService.printSuccessMessage("Số lượng sản phẩm không đủ hoặc không đúng định dạng !");
            }
            else if (detail.amount > detail.product.qty){
                detail.amount = detail.product.qty;
                this._notiService.printSuccessMessage("Số lượng sản phẩm không đủ hoặc không đúng định dạng !");
            }
            else
            {
                
            }
            
        }
        this.calcTotalMoney();
    }
    
}