import {IProduct} from '../model/IProduct'
import {IDelivery} from '../model/IDelivery'
export interface IOrder{
    totalMoney: number,
    totalAmount: number,
    statement: string,
    orderDetail: IOrderDetail[],
    typePayment: number,
    cost: string,
    delivery: IDelivery,
}
export interface IOrderDetail{
    price: number,
    product: IProduct,
    amount: number,

}