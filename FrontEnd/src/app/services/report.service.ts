import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpInterceptor } from '../http.interceptor'
import {Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {AppConfig} from '../app.config'
import {IYearReport} from '../model/IReport'





@Injectable()
export class ReportService{
    url_api =`${AppConfig.hostapi}/api/report`;

    constructor(private http: HttpInterceptor){}
    getReport(year: number, month_start: number, month_end: number):Observable<IYearReport[]>{
       return this.http.get(`${this.url_api}?years=${year}&month_begin=${month_start}&month_end=${month_end}`).map((res: Response) => {
            return <IYearReport[]>res.json();
       } )
       .catch(this.handleError);
    }

    private handleError(error: Response)
    {
        console.log(error);
        return Observable.throw(error.json() || 'Server error');
    }
}