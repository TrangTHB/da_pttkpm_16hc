import { Injectable } from '@angular/core';
import { Publisher  } from '../model/Publisher';
import { Http, Response } from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import {HttpInterceptor} from '../http.interceptor'
import {AppConfig} from '../app.config'
import { Observable } from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PublisherService{
    url_api =`${AppConfig.hostapi}/api/publishers`;
    headers = new Headers({ 'Content-Type': 'application/json' });
     options = new RequestOptions({ headers: this.headers });

    

    constructor(private http: HttpInterceptor){}

    getListCatApi(){
       return this.http.get(this.url_api).map(res => res.json());

    }
    
    getByID(id: string){
        let url_ID = this.url_api + '/' + id;
        console.log(url_ID);
        return this.http.get(url_ID).map(res => res.json());
    }

    deleteOne(id:string):Observable<void>{
        let url_ID = this.url_api + '/' + id;

        return this.http.delete(
           url_ID,
            this.options
        ).map(res =>{
            return;
        });
       
    }

    modifyOne(publisher: Publisher):Observable<Publisher>{
        let url_ID = this.url_api + '/' +publisher._id;
       return this.http.patch(
            url_ID,
            JSON.stringify(publisher),
            this.options
        ).map(res => {
            return res.json();
        });  
    }

    
    createNew(publisher: Publisher):Observable<Publisher>{
         return this.http.post(
            this.url_api,
            JSON.stringify(publisher),
            this.options
        ).map(res => {
            return res.json();
        });
    }
}