import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpInterceptor } from '../http.interceptor'
import {Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {IProduct} from '../model/IProduct'
import {IProduct as ProductCreateModel} from '../pages/product.component/product-create.component/productCreateModel'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {AppConfig} from '../app.config'


@Injectable()
export class ProductService{
    url_api =`${AppConfig.hostapi}/api/products`;

    constructor(private http: HttpInterceptor){}
    getList():Observable<IProduct[]>{
       return this.http.get(this.url_api).map((res: Response) => {
            return <IProduct[]>res.json();
       } )
       .catch(this.handleError);
    }
      getListForSale():Observable<IProduct[]>{
       return this.http.get(this.url_api + "?qty=1").map((res: Response) => {
            return <IProduct[]>res.json();
       })
       .catch(this.handleError);
    }
    getListForImport():Observable<IProduct[]>{
       return this.http.get(this.url_api).map((res: Response) => {
            return <IProduct[]>res.json();
       })
       .catch(this.handleError);
    };
    deleteUser(productId: string): Observable<any>{
        return this.http.delete(this.url_api + '/' + productId).map((res:Response) => {
            return res;
        })
        .catch(this.handleError);
    }
    create(model: ProductCreateModel): Observable<ProductCreateModel>{
        var formData = new FormData();
        if (model.img_main != null)
        {
            formData.append('img_main', model.img_main);
        }
        formData.append('model', JSON.stringify(model));
        var headers = new Headers();
        headers.append('enctype', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        model.img_main = null;

        return this.http.post(this.url_api, formData)
        .map((res: Response) => <ProductCreateModel>res.json())
        .catch(this.handleError);
    }
    getDetail(_id :string): Observable<ProductCreateModel>{
       return this.http.get(`${this.url_api}/${_id}`)
        .map((res: Response) => <ProductCreateModel>res.json())
        .catch(this.handleError);
    }
    update(model: ProductCreateModel): Observable<any>{
        var formData = new FormData();
        if (model.img_main != null)
        {
            formData.append('img_main', model.img_main);
        }
        formData.append('model', JSON.stringify(model));
        return this.http.patch(`${this.url_api}/${model._id}`, formData)
        .map((res: Response) => res.json())
        .catch(this.handleError);
    }
    
    private handleError(error: Response)
    {
        console.log(error);
        return Observable.throw(error.json() || 'Server error');
    }
}