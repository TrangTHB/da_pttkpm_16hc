import { Injectable } from '@angular/core';
import { Category } from '../model/category';
import { Http, Response } from '@angular/http';
import { HttpInterceptor } from '../http.interceptor'
import {Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {AppConfig} from '../app.config'


@Injectable()
export class CatService{
    url_api =`${AppConfig.hostapi}/api/categories`;

    constructor(private http: HttpInterceptor){}


    getListCatApi():Observable<Category[]>{
       return this.http.get(this.url_api).map(res => {
            return res.json();
       } )
       .catch(this.handleError);
    }
    
    getCategoryByID(id: string){
        let url_ID = this.url_api + '/' + id;
        return this.http.get(url_ID).map(res => res.json()).catch(this.handleError);
    }

    deleteOneCategory(id:string): Observable<void>{
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let url_ID = this.url_api + '/' + id;

        return this.http.delete(
           url_ID,
            options
        ).map(res => {
            return;
        }).catch(this.handleError);
       
    }

    modifyOneCategory(category: Category):Observable<Category>{
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        let url_ID = this.url_api + '/' + category._id;
       return this.http.put(
            url_ID,
            JSON.stringify(category),
            options
        ).map(res => {
            return res.json();
        }).catch(this.handleError);

    }

    
    createNewCategory(data: any):Observable<Category>{

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

         return this.http.post(
            this.url_api,
            JSON.stringify(data),
            options
        ).map(res =>{
            return res.json();
        }).catch(this.handleError);
    }

    private handleError(error: Response)
    {
        console.log(error);
        return Observable.throw(error.json() || 'Server error');
    }
}