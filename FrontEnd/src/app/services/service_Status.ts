import { Injectable } from '@angular/core';
import { Status  } from '../model/Status';
import { Http, Response } from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import {HttpInterceptor} from '../http.interceptor'
import {AppConfig} from '../app.config'
import { Observable } from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class StatusService{
    url_api =`${AppConfig.hostapi}/api/status`;
    

    constructor(private http: HttpInterceptor){}

    getListApi():Observable<Status[]>{
       return this.http.get(this.url_api).map(res => {
            return res.json();
       } )
       .catch(this.handleError);

    }

    private handleError(error: Response)
    {
        console.log(error);
        return Observable.throw(error.json() || 'Server error');
    }
    
  
}