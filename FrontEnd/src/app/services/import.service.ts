import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpInterceptor } from '../http.interceptor'
import {Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {IImport} from '../import_good/importModel'

import {IImport as Import, IImportDetail as ImportDetail} from '../model/IImport'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {AppConfig} from '../app.config'


@Injectable()
export class ImportService{
    url_api =`${AppConfig.hostapi}/api/import_goods`;

    constructor(private http: HttpInterceptor){}
    public delete(id: string) : Observable<any>{
        return this.http.delete(`${this.url_api}/${id}`).map((res) => {
            return res.json();
        })
        .catch(this.handleError);
    }
    public getList(): Observable<Import[]>{
        return this.http.get(this.url_api).map((res: Response) => {
            return  <Import[]>res.json();
        })
        .catch(this.handleError);
    }
    public getImportDetail(id: string): Observable<Import>{
        return this.http.get(this.url_api + "/" + id)
            .map(res => <Import>res.json())
            .catch(this.handleError);
    }
    public updateDetail(model: ImportDetail): Observable<any>{
        return this.http.patch(this.url_api + "/updateDetail"+"/" +model._id, model)
            .map(res =>  res.json())
            .catch(this.handleError);
    }
    public create(Import: IImport): Observable<any>{
        return this.http.post(this.url_api, Import).map((res: Response) => {
            return res.json();
        })
        .catch(this.handleError);
    }
    
    private handleError(error: Response)
    {
        console.log(error);
        return Observable.throw(error.json() || 'Server error');
    }
}