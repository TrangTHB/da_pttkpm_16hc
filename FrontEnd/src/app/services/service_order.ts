//Test trả về kiểu Order luôn
//Nếu làm cách này thì map sẽ không gọi kịp nên luôn trả về null


import { Injectable } from '@angular/core';
import { Order } from '../model/Order';
import { Status } from '../model/Status';

import { OrderDetail } from '../model/OrderDetail';
import { HttpInterceptor } from '../http.interceptor'
import { Observable } from 'rxjs/Observable';



import { Http, Response } from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {AppConfig} from '../app.config';
import {IOrder} from '../sale/salemodel'


@Injectable()
export class OrderService{
   url_api =`${AppConfig.hostapi}/api/orders`;

    

    constructor(private http:HttpInterceptor){}

    // convertToOrders(data: any): Order[]{
    //     console.log(data);
    //     let result : Order[] = [];
    //     for(let order of data){
    //        let user = new User(order.user._id, order.user.name);

    //        //Order details
    //        let orderDetails: OrderDetail[] = [];
    //        for(let detail of data.orderDetail){
    //            let product : Product = new Product(detail.product._id, detail.product.name);
    //            let order: OrderDetail = new OrderDetail(detail._id,product);
    //            orderDetails.push(order);
    //        }

    //        let orderResult: Order = new Order(order._id,order.totalMoney,user,orderDetails);
    //        result.push(orderResult);

    //     }
    //     return result;
    // }
    
    getOrders(): Observable<Order[]>{
        return this.http.get(this.url_api).map(res => {
            return res.json();
       } )
       .catch(this.handleError);
    }
    createOrder(order: IOrder): Observable<any>{
        return this.http.post(this.url_api, order).map(res => {
            return res.json;
        })
        .catch(this.handleError);
    }

    getOrderDetails(id: string):Observable<OrderDetail[]>{
        return this.http.get(this.url_api+ '/'+ id).map(res => {
            return res.json();
       } )
       .catch(this.handleError);
    }
    deleteOrder(order : Order): Observable<void>{
         let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let url_ID = this.url_api + '/' + order._id;

        return this.http.delete(
           url_ID,
            options
        ).map(res => {
            return;
        }).catch(this.handleError);
    }
    updateOrder(order : Order,status : Status) :Observable<Order>{
        let orderUpdate = {} as any;
        orderUpdate._id = order._id;
        orderUpdate.statement = status._id;

         let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

       return this.http.patch(
            this.url_api+'/'+ orderUpdate._id,
            JSON.stringify(orderUpdate),
            options
        ).map(res => {
            return res.json();
        }).catch(this.handleError);

    }
     private handleError(error: Response)
    {
        console.log(error);
        return Observable.throw(error.json() || 'Server error');
    }
}