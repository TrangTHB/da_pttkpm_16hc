import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpInterceptor } from '../http.interceptor'
import {Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {ICost} from '../model/ICost'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {AppConfig} from '../app.config'


@Injectable()
export class CostService{
    url_api =`${AppConfig.hostapi}/api/cost`;

    constructor(private http: HttpInterceptor){}
    getList():Observable<ICost[]>{
       return this.http.get(this.url_api).map((res: Response) => {
            return <ICost[]>res.json();
       } )
       .catch(this.handleError);
    }
    delete(costId: string): Observable<any>{
        return this.http.delete(this.url_api + "/" + costId)
        .map((res: Response) => {
            return res.json();
        })
        .catch(this.handleError);
    }
    getDetail(costId: string): Observable<ICost>{
        return this.http.get(this.url_api + '/' + costId)
        .map((res: Response) => res.json())
        .catch(this.handleError);
    }
    update(model: ICost): Observable<any>{
        return this.http.patch(this.url_api + '/' + model._id, model)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    create(model: ICost) : Observable<any>{
        console.log(model.price);
        return this.http.post(this.url_api, {name: model.name, price: model.price})
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    private handleError(error: Response)
    {
        console.log(error);
        return Observable.throw(error.json() || 'Server error');
    }
}