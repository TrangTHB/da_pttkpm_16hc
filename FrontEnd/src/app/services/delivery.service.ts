import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpInterceptor } from '../http.interceptor'
import {Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {IDelivery} from '../model/IDelivery'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {AppConfig} from '../app.config'


@Injectable()
export class DeliverySerivce{
    url_api =`${AppConfig.hostapi}/api/delivery`;

    constructor(private http: HttpInterceptor){}
    getList():Observable<IDelivery[]>{
       return this.http.get(this.url_api).map((res: Response) => {
            return <IDelivery[]>res.json();
       } )
       .catch(this.handleError);
    }
    delete(Id: string): Observable<any>{
        return this.http.delete(this.url_api + "/" + Id)
        .map((res: Response) => {
            return res.json();
        })
        .catch(this.handleError);
    }
    getDetail(costId: string): Observable<IDelivery>{
        return this.http.get(this.url_api + '/' + costId)
        .map((res: Response) => res.json())
        .catch(this.handleError);
    }
    update(model: IDelivery): Observable<any>{
        return this.http.patch(this.url_api + '/' + model._id, model)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    
    private handleError(error: Response)
    {
        console.log(error);
        return Observable.throw(error.json() || 'Server error');
    }
}