import { Component, OnInit } from '@angular/core';

import {ICost} from '../../../model/ICost'
import {CostService} from '../../../services/cost.service'
import {NotificationService} from '../../../services/notification.service'
declare var jQuery:any;

@Component({
  templateUrl: './cost-list.component.html',
  providers: [NotificationService],
  moduleId: module.id
})

export class CostListComponent  {
    lstCost: ICost[];
    deleteItem: ICost;
    public IsDelete: boolean = false;
    constructor(private _costService: CostService, private _notiService: NotificationService){};
    handleDeleteDone(rs: boolean){
        this.LoadData();
    }
    LoadData(): void{
         this.lstCost = undefined;
         this._costService.getList()
                .subscribe(
                    data => {
                        this.lstCost = data;
                    },
                    (error: any) => console.log("Lỗi xảy ra ở HTTP service")
                    );
    }
    ngOnInit(): void {
            this.LoadData();
    }
    ngAfterViewChecked(){
       jQuery('#table-list').DataTable();
    }
   delete(item: ICost): void{
           this._notiService.openConfirmationDialog('Bạn có muốn xóa phí vận chuyển này?',
        () => {
            this._costService.delete(item._id)
                .subscribe(() => {
                    this.lstCost.splice(this.lstCost.indexOf(item), 1);
                    this._notiService.printSuccessMessage('Xóa phí vận chuyển thành công');
                },error =>{
                     if (error.lstErr){
                        this._notiService.printErrorMessage('Xóa không thành công: ' +  error.lstErr.join(', '));
                    }
                    else
                    {
                        this._notiService.printErrorMessage('Xóa không thành công');
                    }


                });

        });
   }
   
   
}
