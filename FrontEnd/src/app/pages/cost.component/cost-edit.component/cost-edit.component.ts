import {Component, OnInit} from '@angular/core'
import {ICost} from '../../../model/ICost'
import {ActivatedRoute, Router} from '@angular/router'
import { Subscription } from 'rxjs/Rx';
import {CostService} from '../../../services/cost.service'
import {NotificationService} from '../../../services/notification.service';

@Component({
    moduleId: module.id,
    templateUrl: './cost-edit.component.html',
    styleUrls: ['./cost-edit.component.css'],
    providers: [NotificationService]
})
export class CostEditComponent implements OnInit{
     public messages: string[];
     public model: ICost;
     subscription: Subscription;
     isCreating: boolean = false;
     constructor(private _route: ActivatedRoute, private _costService: CostService, private _notiService: NotificationService){

     }
     ngOnInit(): void{
        this.subscription = this._route.params.subscribe(params => {
            var id: string = this._route.snapshot.params['costID'];
             this._costService.getDetail(id).subscribe(cost => {
                this.model = cost;
             }, err => {
                 console.log(err);
             })
        });
     }
     
     update(){
         this.isCreating = true;
         this.messages = [];
         this._costService.update(this.model)
         .subscribe(success => {
             console.log(success);
            this.isCreating = false;
            this._notiService.printSuccessMessage('Cập nhật phí vận chuyển thành công');
            }, err => {
             console.log(err);
             this.isCreating = false;
             this._notiService.printErrorMessage('Cập nhật phí vận chuyển thành công không thành công');
             this.messages = err.lstErr;
         })

     }
    
}