import {Component, OnInit} from '@angular/core'
import {ICost} from '../../../model/ICost'
import {ActivatedRoute, Router} from '@angular/router'
import { Subscription } from 'rxjs/Rx';
import {CostService} from '../../../services/cost.service'
import {NotificationService} from '../../../services/notification.service';

@Component({
    moduleId: module.id,
    templateUrl: './cost-add.component.html',
    styleUrls: ['./cost-add.component.css'],
    providers: [NotificationService]
})
export class CostAddComponent implements OnInit{
     public messages: string[];
     public model: ICost;
     subscription: Subscription;
     isCreating: boolean = false;
     constructor(private _route: ActivatedRoute, private _costService: CostService, private _notiService: NotificationService){
       this.initModel();
     }
     initModel(){
         this.model = {
            name: '',
            price: 10000,
            _id: ''
        };
     }
     ngOnInit(): void{
     }
     
     create(){
         this.isCreating = true;
         this.messages = [];
         this._costService.create(this.model)
         .subscribe(success => {
             console.log(success);
            this.isCreating = false;
            this._notiService.printSuccessMessage('Thêm phí vận chuyển thành công');
            this.initModel();

        }, err => {
            
             console.log(err);
             this.isCreating = false;
             this._notiService.printErrorMessage('Thêm phí vận chuyển thành công không thành công');
             this.messages = err.lstErr;
         })

     }
    
}