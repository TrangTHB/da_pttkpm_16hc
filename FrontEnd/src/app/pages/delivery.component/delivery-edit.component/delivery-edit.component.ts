import {Component, OnInit} from '@angular/core'
import {IDelivery} from '../../../model/IDelivery'
import {ActivatedRoute, Router} from '@angular/router'
import { Subscription } from 'rxjs/Rx';
import {DeliverySerivce} from '../../../services/delivery.service'
import {NotificationService} from '../../../services/notification.service';

@Component({
    moduleId: module.id,
    templateUrl: './delivery-edit.component.html',
    styleUrls: ['./delivery-edit.component.css'],
    providers: [NotificationService]
})
export class DeliveryEditComponent implements OnInit{
     public messages: string[];
     public model: IDelivery;
     subscription: Subscription;
     isCreating: boolean = false;
     constructor(private _route: ActivatedRoute, private _deliveryService: DeliverySerivce, private _notiService: NotificationService){

     }
     ngOnInit(): void{
        this.subscription = this._route.params.subscribe(params => {
            var id: string = this._route.snapshot.params['deliveryID'];
             this._deliveryService.getDetail(id).subscribe(cost => {
                this.model = cost;
             }, err => {
                 console.log(err);
             })
        });
     }
     
     update(){
         this.isCreating = true;
         this.messages = [];
         this._deliveryService.update(this.model)
         .subscribe(success => {
             console.log(success);
            this.isCreating = false;
            this._notiService.printSuccessMessage('Cập nhật phí vận chuyển thành công');
            }, err => {
             console.log(err);
             this.isCreating = false;
             this._notiService.printErrorMessage('Cập nhật phí vận chuyển không thành công');
             this.messages = err.lstErr;
         })

     }
    
}