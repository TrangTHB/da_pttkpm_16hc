import { Component, OnInit } from '@angular/core';

import {IDelivery} from '../../../model/IDelivery'
import {DeliverySerivce} from '../../../services/delivery.service'
import {NotificationService} from '../../../services/notification.service'
declare var jQuery:any;

@Component({
  templateUrl: './delivery.management.component.html',
  providers: [NotificationService],
  moduleId: module.id
})

export class DeliveryManagementComponent  {
    lstDelivery: IDelivery[];
    deleteItem: IDelivery;
    public IsDelete: boolean = false;
    constructor(private _deliveryService: DeliverySerivce, private _notiService: NotificationService){};
    handleDeleteDone(rs: boolean){
        this.LoadData();
    }
    LoadData(): void{
         this.lstDelivery = undefined;
         this._deliveryService.getList()
                .subscribe(
                    data => {
                        this.lstDelivery = data;
                    },
                    (error: any) => console.log("Lỗi xảy ra ở HTTP service")
                    );
    }
    ngOnInit(): void {
            this.LoadData();
   }
    ngAfterViewChecked(){
       jQuery('#table-list').DataTable();
    }
   delete(item: IDelivery): void{
           this._notiService.openConfirmationDialog('Bạn có muốn xóa thông tin giao hàng này?',
        () => {
            this._deliveryService.delete(item._id)
                .subscribe(() => {
                    this.lstDelivery.splice(this.lstDelivery.indexOf(item), 1);
                    this._notiService.printSuccessMessage('Xóa thông tin giao hàng thành công');
                },error =>{
                    if (error.lstErr){
                        this._notiService.printErrorMessage('Xóa không thành công: ' +  error.lstErr.join(', '));
                    }
                    else
                    {
                        this._notiService.printErrorMessage('Xóa không thành công');
                    }
                });

        });
   }
   
   
}
