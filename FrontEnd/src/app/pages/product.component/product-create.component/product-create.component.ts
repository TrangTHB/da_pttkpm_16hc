import {Component, OnInit} from '@angular/core'
import {ProductService} from '../../../services/product.service'
import {Router} from '@angular/router'
import {IProduct} from './productCreateModel'
import {PublisherService} from '../../../services/service_Publisher'
import {IPublisher} from '../../../model/IProduct'
import {CatService} from '../../../services/service_cat'
import {NotificationService} from '../../../services/notification.service'
interface ItemSelect{
    id: string,
    name: string
}

@Component({
    templateUrl: './product-create.component.html',
    styleUrls: ['./product-create.component.css'],
})
export class ProductCreateComponent implements OnInit {
    optionsModel: number[];
    public model: IProduct;
    public success: boolean = false;
    public messages: string[];
    public isCreating: boolean = false;
    public publishers: IPublisher[] = [];
    public itemCatSelect: ItemSelect[] = [];

    constructor(private router: Router, private _productService: ProductService,
                private _publisherService: PublisherService,
                private _catService: CatService,
                private _notiService: NotificationService){
        
                this.setInitModel();
    }
    setInitModel(){
        this.model = {
            _id: '',
            name: '',
            category: [],
            cost: 0,
            img_main: null,
            promotion_price: 0,
            publisher: '-1',
            retail_price: 0,
        };
    }
   onChangeFile(files: any){
       this.model.img_main = files[0];
       console.log(this.model.img_main);

   }
    ngOnInit(){
        this._publisherService.getListCatApi().subscribe(h => {
            this.publishers = h;
        }, error => {
            console.log(error);
        })
        this._catService.getListCatApi().subscribe(h => {
            this.itemCatSelect = h.map(h => {
                let item: ItemSelect = {
                    id: h._id,
                    name: h.name
                }
                return item;
            })
        }, error => {
            console.log(error);
        })
        
    }
    
    addProduct(): void{
        console.log(this.model);
        this.isCreating = true;
        this.messages = [];
        this._productService.create(this.model)
        .subscribe(data => {
            this.success = true;
            this.isCreating = false;
            this._notiService.printSuccessMessage("Thêm sản phẩm thành công");
            this.setInitModel();
        }, err => {
            this.isCreating = false;
            this._notiService.printErrorMessage("Thêm sản phẩm không thành công");
            this.messages = err.lstErr;

        });
    }
}