export interface IProduct{
    _id: string,
    name: string,
    retail_price: number,
    promotion_price: number,
    cost: number,
    publisher: string,
    img_main: any,
    category: string[]
}