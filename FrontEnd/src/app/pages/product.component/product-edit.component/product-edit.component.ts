import {Component, OnInit} from '@angular/core'
import {IProduct} from '../product-create.component/productCreateModel'
import {ActivatedRoute, Router} from '@angular/router'
import { Subscription } from 'rxjs/Rx';
import {ProductService} from '../../../services/product.service'
import {NotificationService} from '../../../services/notification.service';
import {CatService} from '../../../services/service_cat'
import {PublisherService} from '../../../services/service_Publisher'
import {IPublisher} from '../../../model/IProduct'
interface ItemSelect{
    id: string,
    name: string
}

@Component({
    moduleId: module.id,
    templateUrl: './product-edit.component.html',
    styleUrls: ['./product-edit.component.css'],
    providers: [NotificationService]
})
export class ProductEditComponent implements OnInit{
     public messages: string[];
     public model: IProduct;
     subscription: Subscription;
     isCreating: boolean = false;
     optionsModel: number[];
     public publishers: IPublisher[] = [];
     public itemCatSelect: ItemSelect[] = [];
     constructor(
        private _route: ActivatedRoute,
        private _productService: ProductService, 
        private _notiService: NotificationService,
        private _publisherService: PublisherService,
        private _catService: CatService){

     }
     ngOnInit(): void{
        this.subscription = this._route.params.subscribe(params => {
            var id: string = this._route.snapshot.params['productID'];
             this._productService.getDetail(id).subscribe(h => this.model = h, error => {
                 console.log(error);
             })
               this._publisherService.getListCatApi().subscribe(h => {
                    this.publishers = h;
                }, error => {
                    console.log(error);
                })
                this._catService.getListCatApi().subscribe(h => {
                    this.itemCatSelect = h.map(h => {
                        let item: ItemSelect = {
                            id: h._id,
                            name: h.name
                        }
                        return item;
                    })
                }, error => {
                    console.log(error);
                })
        
        });
     }
      onChangeFile(files: any){
       this.model.img_main = files[0];
       console.log(this.model.img_main);

   }
     
     update(){
         this.isCreating = true;
         this.messages = [];
         this._productService.update(this.model)
         .subscribe(success => {
             console.log(success);
            this.isCreating = false;
            this._notiService.printSuccessMessage('Cập nhật sản phẩm thành công');
            }, err => {
             console.log(err);
             this.isCreating = false;
             this._notiService.printErrorMessage('Cập nhật sản phẩm không thành công');
             this.messages = err.lstErr;
         })

     }
    
}