import { Component, OnInit } from '@angular/core'
import {ProductService} from '../../../services/product.service'
import {NotificationService} from '../../../services/notification.service'
import {IProduct} from '../../../model/IProduct'
declare var jQuery:any;

@Component({
  templateUrl: './product-list.component.html',
  providers: [NotificationService],
  moduleId: module.id
})
export class ProductListComponent  {
    lstProduct: IProduct[];
    public IsDelete: boolean = false;
    constructor(private _productService: ProductService, private _notiService: NotificationService){};
    handleDeleteDone(rs: boolean){
        this.LoadData();
    }
    LoadData(): void{
         this.lstProduct = undefined;
         this._productService.getList()
                .subscribe(
                    data => {
                        this.lstProduct = data;
                        console.log(data);
                    },
                    (error: any) => console.log("Lỗi xảy ra ở HTTP service")
                    );
    }
    ngOnInit(): void {
            this.LoadData();
   }
    ngAfterViewChecked(){
       jQuery('#table-list').DataTable();
    }
   deleteProduct(item: IProduct): void{
           this._notiService.openConfirmationDialog('Bạn có muốn xóa danh mục này?',
        () => {
            this._productService.deleteUser(item._id)
                .subscribe(h => {
                    this.lstProduct.splice(this.lstProduct.indexOf(item), 1);
                    this._notiService.printSuccessMessage('Xóa sản phẩm thành công');
                },error =>{
                    if (error.lstErr){
                        this._notiService.printErrorMessage('Xóa không thành công: ' +  error.lstErr.join(', '));
                    }
                    else
                    {
                        this._notiService.printErrorMessage('Xóa không thành công');
                    }
                });

        });
   }
   
   
}
