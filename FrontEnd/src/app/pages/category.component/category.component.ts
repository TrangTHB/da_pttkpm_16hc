import { Component, OnInit,ViewChild } from '@angular/core';

import {Category} from '../../model/category';
import {CatService} from '../../services/service_cat';
import {NotificationService} from '../../services/notification.service';
import {ItemsService} from '../../services/items.service';


declare var jQuery:any;

@Component({
    moduleId: module.id,
  templateUrl: 'category.component.html',
  providers: [CatService,NotificationService,ItemsService]
})

export class CategoryComponent  {

    
    list_Category: Category[];

    constructor(private service_category: CatService,private notificationService: NotificationService,
                private itemService : ItemsService){};
    
    loadCategories(){
      this.service_category.getListCatApi()
                .subscribe(
                    data => this.list_Category = data,
                    (error: any) => console.log("Lỗi xảy ra ở HTTP service")
                    );      
    }
    ngAfterViewChecked(){
       jQuery('#table-list').DataTable();
    }
    ngOnInit(): void {
            this.loadCategories();
        }
    delete(cat:Category){

            this.notificationService.openConfirmationDialog('Bạn có muốn xóa danh mục này?',
        () => {
            this.service_category.deleteOneCategory(cat._id)
                .subscribe(() => {
                    this.itemService.removeItemFromArray(this.list_Category,cat);
                    this.notificationService.printSuccessMessage('Xóa thành công');
                },error =>{
                    if (error.lstErr){
                        this.notificationService.printErrorMessage('Xóa không thành công: ' +  error.lstErr.join(', '));
                    }
                    else
                    {
                        this.notificationService.printErrorMessage('Xóa không thành công');
                    }
                });

        });
    }

    // list_Category: Category[];
    deleteItem: Category;
    public IsDelete: boolean = false;
    // constructor(private service_category: CatService){};
    // handleDeleteDone(rs: boolean){
    //     this.LoadData();
    // }
//     LoadData(): void{
//          this.list_Category = undefined;
//          this.service_category.getListCatApi()
//                 .subscribe(
//                     data => this.list_Category = data,
//                     (error: any) => console.log("Lỗi xảy ra ở HTTP service")
//                     );
//     }
// //     ngOnInit(): void {
//             this.LoadData();
//    }
//    showDialogDelete(item: Category): void{
//         this.deleteItem = item;
//         jQuery('#myModal').modal();
//    }
//    confirmDelete(){
//        if (!this.deleteItem) return;

//         this.IsDelete = true;
//         this.service_category.deleteOneCategory(this.deleteItem._id).subscribe(success => {
//             this.IsDelete = false;
//             jQuery('#myModal').modal('hide');
//             this.LoadData();
            
//         }, err => {
//             console.log(err)
//             this.IsDelete = false;
//             });
        
//     }
   
// >>>>>>> 1ca60de03f723ac4b39d7e8ef1505cc15ac8c275
}

