import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Router } from '@angular/router';
import {CatService} from '../../services/service_cat';
import {NotificationService} from '../../services/notification.service';


import {Category} from '../../model/category';
import { NgForm } from '@angular/forms';

@Component({
    moduleId: module.id,
     selector: 'modify-form',
  templateUrl: 'modifyCategory.compoment.html',
  providers: [CatService,NotificationService],
  styleUrls: ['./modifyCategory.component.css'],
})

export class ModifyCategoryComponent implements OnInit {
    lstErr: string[];
    categoryID : string;
    isEditting: boolean = false;
    category : Category;
        ngOnInit(): void {
           this.route.params.forEach(
               (param: Params) => {
               this.categoryID =  param['id'];
               console.log(this.categoryID);
               if(this.categoryID){
                    this.service_category
                        .getCategoryByID(this.categoryID)
                        .subscribe(
                            data => this.category = new Category(data._id,data.name),
                             (error: any) => console.log(error)
                        );
               }
           });
        }



    constructor(private service_category: CatService, private router: Router,
                	private route: ActivatedRoute,private notificationService: NotificationService){};
    
    onSubmit(){
        this.isEditting = true;
        this.lstErr = undefined;

        this.service_category.modifyOneCategory(this.category)
            .subscribe(()=>{
                this.isEditting = false;
                this.notificationService.printSuccessMessage("Sửa thành công");
            },err=>{
                console.log(err);
                this.lstErr = err.lstErr;
                this.isEditting = false;
              this.notificationService.printErrorMessage("Sửa thất bại");
            });

    }     

    get diagnostic(){
        return JSON.stringify(this.category);
    }
    back(){
      this.router.navigate(['/categories']);
    }
  }

