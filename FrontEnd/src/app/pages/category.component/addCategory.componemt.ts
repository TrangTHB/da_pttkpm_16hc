import { Component, } from '@angular/core';
import {CatService} from '../../services/service_cat';
import { NgForm } from '@angular/forms';
import {Router } from '@angular/router';
import {NotificationService} from '../../services/notification.service';

class AddCategoryModel
{
   public name: string;
}


@Component({
    moduleId: module.id,
     selector: 'create-form',
  templateUrl: 'addCategory.componemt.html',
  providers: [CatService,NotificationService],
  styleUrls: ['./addCategory.componemt.css'],
 
})


export class AddCategoryComponent  {


    public model: AddCategoryModel;
    public lstErr: string[];
    public isAdding: boolean = false;

    constructor(private service_category: CatService, private router: Router,
          private notificationService : NotificationService){
               this.model = {name: ''};
          };

    onSubmit(form: NgForm){
       this.isAdding = true;
       this.lstErr = [];
        this.service_category.createNewCategory(form.value)
            .subscribe(()=>{
              this.isAdding = false;
              this.notificationService.printSuccessMessage("Thêm thành công");
              this.router.navigate(['/categories']);
            },err=>{
               this.isAdding = false;
              console.log(err);
              this.lstErr = err.lstErr;
            });
  
    }
    back(){
      this.router.navigate(['/categories']);
    }





  
  }

