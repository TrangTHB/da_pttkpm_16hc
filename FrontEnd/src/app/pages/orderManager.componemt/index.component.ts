import { Component, OnInit,ViewChild } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';

import {Order} from '../../model/Order';
import {Status} from '../../model/Status';

import {OrderDetail} from '../../model/OrderDetail';

import {NotificationService} from '../../services/notification.service';
import {OrderService} from '../../services/service_order';
import {StatusService} from '../../services/service_Status';
import {ItemsService} from '../../services/items.service';

declare var jQuery:any;

@Component({
  templateUrl: 'index.component.html',
  providers: [OrderService,NotificationService,StatusService,ItemsService]
})

export class OrderComponent  {

    //View modal
    @ViewChild('childModal') public childModal: ModalDirective;
    @ViewChild('modifyModal') public modifyModal: ModalDirective;
    
    orders: Order[];
    status: Status[];
    orderSelected: Order = null;
    orderDetails: OrderDetail[];
    
     @ViewChild('modal') modal: any;
    selectedOrderLoaded : boolean = false;
    selectedModifyLoaded : boolean = false;


    statusSelected: Status = null;
    constructor(private service_order: OrderService,private notificationService: NotificationService,
            private service_status : StatusService,private itemService : ItemsService){};

    ngOnInit(): void {
            this.LoadOrder();
             this.service_status.getListApi().subscribe(
                data => this.status = data,
                (error : any) => console.log("Lỗi xảy ra ở HTTP service")
             );
               
        }


    ngAfterViewChecked(){
        jQuery('#table-list').DataTable();
        jQuery('#orders-list').DataTable();
        
    }
    get diagnostic(){

        return JSON.stringify(this.orders);
    }

    LoadOrder(){
                this.service_order.getOrders().subscribe(
                  data => this.orders = data,
                    (error: any) => console.log("Lỗi xảy ra ở HTTP service")
                    );
    }
    viewOrderDetails(order :Order){
        this.orderSelected = order;
        this.service_order.getOrderDetails(this.orderSelected._id).subscribe(
            data => {
            this.orderDetails = data;
                console.log(data);
            this.selectedOrderLoaded = true;
               this.childModal.show(); 
            },
            (error: any) => {console.log("Loi");
            this.notificationService.printErrorMessage('Có lỗi trong quá trình xử lý' + error);
        }
        );
        
        
    }
    Delete(order: Order){
         this.notificationService.openConfirmationDialog('Bạn có muốn xóa dơn hàng này?',
        () => {
             this.service_order.deleteOrder(order).subscribe(
            () => {
                this.itemService.removeItemFromArray(this.orders,order);

                this.notificationService.printSuccessMessage('Xóa thành công ');
                
            },
            (error: any) => {console.log("Lỗi");
              if (error.lstErr){
                        this.notificationService.printErrorMessage('Xóa không thành công: ' +  error.lstErr.join(', '));
                    }
                    else
                    {
                        this.notificationService.printErrorMessage('Xóa không thành công');
                    }
        }
        ); 

        });
        
    }
   findIndexOfStatus(sta : Status){
       for(let i =0 ; i < this.status.length; i++){
           if(this.status[i]._id == sta._id){
               return i;
           }
       }
   }
    modifyOrder(order : Order){

        
        this.orderSelected = order;
        //Set statement tuong ung
        console.log(order.statement);
        //find index in database
        this.statusSelected = this.status[this.findIndexOfStatus(order.statement)];
        this.orderSelected.statement = this.statusSelected;
        
        console.log(this.statusSelected); 
        this.selectedModifyLoaded = true;
        this.modifyModal.show();
    }

    modifyCofirm(){
        console.log(this.orderSelected);
        this.service_order.updateOrder(this.orderSelected,this.statusSelected).subscribe(
            data => {
                this.notificationService.printSuccessMessage('Update thành công');
                this.orderSelected.statement = this.statusSelected;
                this.hideModifyModal();
            },
            (error: any)=>{
                //this.LoadOrder();
                console.log(error);
                this.notificationService.printErrorMessage('Update thất bại');
                this.hideModifyModal();
                
                
            }
        );
    }
    hideModifyModal(){
        this.orderSelected = null;
        this.statusSelected = null;
        this.selectedModifyLoaded = false;
        this.modifyModal.hide();
    }
    hideChildModal(){
        this.selectedOrderLoaded = false;
        this.orderDetails = null;
        this.orderSelected = null;
        this.childModal.hide();
        
    }
}
