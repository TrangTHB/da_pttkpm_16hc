export interface IDeleteUserModel{
    _id: string,
    name: string,
    address: string,
    role: string,
    img: any,
    gender: boolean
}