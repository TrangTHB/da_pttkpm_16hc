import {Component, OnInit} from '@angular/core'
import {IDeleteUserModel} from './editusermodel'
import {ActivatedRoute, Router} from '@angular/router'
import { Subscription } from 'rxjs/Rx';
import {UserService} from '../../../user/user.service'
import {NotificationService} from '../../../services/notification.service';

@Component({
    moduleId: module.id,
    templateUrl: './edit-user.component.html',
    styleUrls: ['./edit-user.component.css'],
    providers: [NotificationService]
})
export class EditUserComponent implements OnInit{
     public messages: string[];
     public model: IDeleteUserModel;
     subscription: Subscription;
     isCreating: boolean = false;
     constructor(private _route: ActivatedRoute, private _userService: UserService, private _notiService: NotificationService){

     }
     ngOnInit(): void{
        this.subscription = this._route.params.subscribe(params => {
              var id: string = this._route.snapshot.params['userID'];
             this._userService.getInfoByUserId(id).subscribe(user => {
                this.model = {
                    name: user.name,
                    _id: user._id,
                    address: user.address,
                    role: user.role,
                    img: null,
                    gender: user.gender
                };
                console.log(this.model);
             }, err => {
                 console.log(err);
             })
        });
     }
     onChangeRole(value: string){
         this.model.role = value;
     }
     updateUser(){
         console.log(this.model);
         this.isCreating = true;
         this._userService.updateUser(this.model)
         .subscribe(success => {
             console.log(success);
            this.isCreating = false;
            this._notiService.printSuccessMessage('Cập nhật người dùng thành công');
            }, err => {
             console.log(err);
             this.messages = err.lstErr;
             this.isCreating = false;
         })

     }
     onChangeGender(value: boolean){
         this.model.gender = value;
     }
     onChangeFile(files: any){
            console.log(files);
            this.model.img = <Array<File>>files[0];
     }
}