import { Component, OnInit } from '@angular/core';

import {IUser} from '../../../model/user'
import {UserService} from '../../../user/user.service'
import {NotificationService} from '../../../services/notification.service'
declare var jQuery:any;

@Component({
  templateUrl: './user-list.component.html',
  providers: [NotificationService],
  moduleId: module.id
})

export class UserListComponent  {
    lstUser: IUser[];
    deleteItem: IUser;
    public IsDelete: boolean = false;
    constructor(private _userService: UserService, private _notiService: NotificationService){};
    handleDeleteDone(rs: boolean){
        this.LoadData();
    }
    LoadData(): void{
         this.lstUser = undefined;
         this._userService.getList()
                .subscribe(
                    data => {
                        this.lstUser = data;
                    },
                    (error: any) => console.log("Lỗi xảy ra ở HTTP service")
                    );
    }
    ngOnInit(): void {
            this.LoadData();
   }
    ngAfterViewChecked(){
       jQuery('#table-list').DataTable();
    }
   deleteUser(item: IUser): void{
           this._notiService.openConfirmationDialog('Bạn có muốn xóa danh mục này?',
        () => {
            this._userService.deleteUser(item._id)
                .subscribe(() => {
                    this.lstUser.splice(this.lstUser.indexOf(item), 1);
                    this._notiService.printSuccessMessage('Xóa người dùng thành công');
                },error =>{
                     if (error.lstErr){
                        this._notiService.printErrorMessage('Xóa không thành công: ' +  error.lstErr.join(', '));
                    }
                    else
                    {
                        this._notiService.printErrorMessage('Xóa không thành công');
                    }
                });

        });
   }
   
   
}
