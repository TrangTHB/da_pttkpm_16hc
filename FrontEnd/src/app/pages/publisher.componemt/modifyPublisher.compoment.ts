import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Router } from '@angular/router';
import {PublisherService} from '../../services/service_Publisher';
import {Publisher} from '../../model/Publisher';
import { NgForm } from '@angular/forms';
import {NotificationService} from '../../services/notification.service';

@Component({
    moduleId: module.id,
     selector: 'modifyPub-form',
  templateUrl: 'modifyPublisher.compoment.html',
  providers: [PublisherService,NotificationService],
    styleUrls: ['./addPublisher.component.css']
})

export class ModifyPublisherComponent implements OnInit {
    publisherID : string;
    publisher : Publisher;
    isEditting: boolean = false;
    public lstErr: string[];
        ngOnInit(): void {
           this.route.params.forEach(
               (param: Params) => {
               this.publisherID =  param['id'];
               if(this.publisherID){
                    this.service_Publisher
                        .getByID(this.publisherID)
                        .subscribe(
                            data => this.publisher = new Publisher(data._id,data.name,data.isdelete),
                             (error: any) => console.log("Lỗi xảy ra ở HTTP service")
                        );
               }
           });
        }



    constructor(private service_Publisher: PublisherService, private router: Router,
                	private route: ActivatedRoute,private notificationService : NotificationService){};
    
    onSubmit(){
        this.isEditting = true;
        
         this.service_Publisher.modifyOne(this.publisher)
            .subscribe(()=>{
                 this.isEditting = false;
                
              this.notificationService.printSuccessMessage("Sửa thành công");
      this.router.navigate(['/publishers']);
              
            },error=>{
                 this.isEditting = false;
                
              console.log(error);
              this.lstErr = error.lstErr;
            });
    }

     back(){
      this.router.navigate(['/publishers']);
    }     
  }

