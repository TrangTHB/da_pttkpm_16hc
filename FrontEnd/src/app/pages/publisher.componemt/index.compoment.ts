import { Component, OnInit,ViewChild} from '@angular/core';

import {Publisher} from '../../model/Publisher';
import {PublisherService} from '../../services/service_Publisher';
import {NotificationService} from '../../services/notification.service';
import {ItemsService} from '../../services/items.service';

declare var jQuery:any;

@Component({
    moduleId: module.id,
  templateUrl: 'index.compoment.html',
  providers: [PublisherService,NotificationService,ItemsService]
})


export class PublisherComponent  {

    list_Publisher: Publisher[];

    constructor(private service_publisher: PublisherService,private notificationService : NotificationService,
                private itemService: ItemsService){};

    ngOnInit(): void {
            this.service_publisher.getListCatApi()
                .subscribe(
                    data => {
                        this.list_Publisher = data;
                         
                    },
                    (error: any) => console.log("Lỗi xảy ra ở HTTP service")
                    );
    }
    ngAfterViewChecked(){
       jQuery('#table-list').DataTable();
    }

    delete(publisher: Publisher){
        this.notificationService.openConfirmationDialog('Bạn có muốn xóa nhà cung cấp này?',
        () => {
            this.service_publisher.deleteOne(publisher._id)
                .subscribe(() => {
                    this.itemService.removeItemFromArray(this.list_Publisher,publisher);
                    this.notificationService.printSuccessMessage('Xóa thành công');
                },error => {
                   if (error.lstErr){
                        this.notificationService.printErrorMessage('Xóa không thành công: ' +  error.lstErr.join(', '));
                    }
                    else
                    {
                        this.notificationService.printErrorMessage('Xóa không thành công');
                    }
                });
        });
         
    }
}
