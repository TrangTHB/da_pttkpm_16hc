import { Component } from '@angular/core';
import {PublisherService} from '../../services/service_Publisher';
import {Publisher} from '../../model/Publisher';
import {NotificationService} from '../../services/notification.service';

import { NgForm } from '@angular/forms';
import {Router } from '@angular/router';

@Component({
    moduleId: module.id,
     selector: 'createPub-form',
  templateUrl: 'addPublisher.componemt.html',
  providers: [PublisherService,NotificationService],
    styleUrls: ['./addPublisher.component.css']
})

export class AddPublisherComponent  {

    publisher : Publisher;
    public isAdding: boolean = false;


    constructor(private service_Pub: PublisherService, private router: Router,
                private notificationService : NotificationService){
        this.publisher = new Publisher(null,null,false);
    };

    onSubmit(){
        this.isAdding = true;
        this.service_Pub.createNew(this.publisher)
            .subscribe(()=>{
              this.isAdding = false;
              this.notificationService.printSuccessMessage("Thêm thành công");
              this.router.navigate(['/publishers']);
            },error=>{
              this.isAdding = false;
              console.log(error);
            });
    }
     back(){
      this.router.navigate(['/publishers']);
    }
  }

