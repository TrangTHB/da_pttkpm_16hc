import { Pipe, PipeTransform } from '@angular/core';
import {IProduct} from '../model/IProduct'
@Pipe({name: 'productFilter'})
export class ProductPipe implements PipeTransform {
  transform(value: IProduct[], keyword: string): IProduct[] {
    if (!keyword || keyword == '') return value;
    keyword = keyword.toLowerCase();
    if (!value || value.length == 0) return value;
    return value.filter(h => {
        if (h.name.toLowerCase().indexOf(keyword) >= 0 || 
        h.publisher.name.toLowerCase().indexOf(keyword) >= 0  ||
        h.productId.toLowerCase().indexOf(keyword) >= 0 ||
        h.category.filter(h => h.name).join().toLowerCase().indexOf(keyword) >= 0)
        {
           return true;
        }
        return false;
    })
  }
}