import {Observable} from 'rxjs/Observable';
import {Http, Request, RequestOptionsArgs, Response, RequestOptions, XHRBackend, Headers} from '@angular/http';
import {AuthService} from './auth/auth.service'
import {Router} from '@angular/router'
import {Injectable} from '@angular/core'

@Injectable()
export class HttpInterceptor extends Http{

     constructor(backend: XHRBackend,
      defaultOptions: RequestOptions, 
      private _router: Router,
      private _authService: AuthService) {
        super(backend, defaultOptions);
    }
     getRequestOptionArgs(options?: RequestOptionsArgs) : RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        let auth = this._authService.getAuth();
        if (auth && auth.isAuth == true && auth.token)
        {
            options.headers.append('Authorization', `Bear ${auth.token}`);
        }
        return options;
    }

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.request(url, this.getRequestOptionArgs(options)));
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.get(url,this.getRequestOptionArgs(options)));
    }

    post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {   
        return this.intercept(super.post(url, body, this.getRequestOptionArgs(options)));
    }

    put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.put(url, body, this.getRequestOptionArgs(options)));
    }

    patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.patch(url, body, this.getRequestOptionArgs(options)));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.delete(url, this.getRequestOptionArgs(options)));
    }

    intercept(observable: Observable<Response>): Observable<Response> {
        return observable.catch((err, source) => {
			if (err.status  == 401 || err.status == 403) {
                this._authService.logOut();
                this._router.navigate(['/login']);

                return Observable.empty();
            } else {
                return Observable.throw(err);
			}
        });

    }
}

export function LoadHttp(backend: XHRBackend, options: RequestOptions, router: Router, authService: AuthService): HttpInterceptor
{
   return new HttpInterceptor(backend, options, router, authService);
}