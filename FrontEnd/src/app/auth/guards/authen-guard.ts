import {Injectable} from '@angular/core'
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router'
import {AuthService} from '../auth.service'
import {UserService} from '../../user/user.service'

//Chặn nếu đăng nhập rồi
@Injectable()
export class UnAuthenGuard implements CanActivate{
    constructor(private authService: AuthService, private router: Router){

    }
    canActivate(route: ActivatedRouteSnapshot): boolean{
        let auth = this.authService.getAuth();
        if (auth && auth.isAuth){
              this.router.navigate(['/home']);
            return false;
        }
        else
        {
            return true;
        }
    }
}

@Injectable()
export class AuthenGuard implements CanActivate{
    constructor(private authService: AuthService, private router: Router){

    }
    canActivate(route: ActivatedRouteSnapshot): boolean{
        let auth = this.authService.getAuth();
        if (auth && auth.isAuth){
            return true;
        }
        else
        {
            this.router.navigate(['/login']);
            return false;
        }
    }
}


@Injectable()
export class AuthenSellerGuard implements CanActivate{
    constructor(private authService: AuthService,
                private router: Router,
                private userService: UserService){
    }
    canActivate(route: ActivatedRouteSnapshot): boolean{
        let auth = this.authService.getAuth();
        if (auth && auth.isAuth){
            let role = auth.role;
            if (role == 'seller' || role == "manager" || role == "admin"){
                return true;
            }
        }
        else
        {
            this.router.navigate(['/login']);
            return false;
        }
    }
}

@Injectable()
export class AuthenManagerGuard implements CanActivate{
    constructor(private authService: AuthService,
                private router: Router,
                private userService: UserService){
    }
    canActivate(route: ActivatedRouteSnapshot): boolean{
        let auth = this.authService.getAuth();
        if (auth && auth.isAuth){
            let role = auth.role;
            if (role == "manager" || role == "admin"){
                return true;
            }
        }
        else
        {
            this.router.navigate(['/login']);
            return false;
        }
    }
}
@Injectable()
export class AuthenAdminGuard implements CanActivate{
    constructor(private authService: AuthService,
                private router: Router){
    }
    canActivate(route: ActivatedRouteSnapshot): boolean{
        let auth = this.authService.getAuth();
        if (auth && auth.isAuth){
            let role = auth.role;
            if (role == "admin"){
                return true;
            }
        }
        else
        {
            this.router.navigate(['/login']);
            return false;
        }
    }
}