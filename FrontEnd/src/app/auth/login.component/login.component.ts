import {Component} from '@angular/core'
import {AuthService} from '../auth.service'
import {Router} from '@angular/router'
import {UserService} from '../../user/user.service'

@Component({
    selector: 'login-cpt',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    public username: string;
    public password: string;
    public message: string;
    public success: boolean = false;
    public lstErr: string[];
    public isloggin: boolean = false;
    constructor(private router: Router, private _authService: AuthService, private _userService: UserService){
        
        console.log(this._authService.getAuth());
    }
    Login(): void{
        this.isloggin = true;
        this.lstErr = undefined;
        this._authService.login(this.username, this.password)
        .subscribe(data => {
            this._authService.setAuth(data);
            this.success = true;
            this.isloggin = false;
            this._userService.getNewInfo();
            setTimeout(()=>{this.router.navigate(['/home'])}, 2000);
        }, err => {
            this.lstErr = err.lstErr;
            this.isloggin = false;
        });
    }
    RedirectHome(): void{
       
    }
    
}