import {Component} from '@angular/core'
import {AuthService} from '../auth.service'
import {Router} from '@angular/router'
import IRegister from './register'
@Component({
    selector: 'login-cpt',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent {
    public model: IRegister;
    public success: boolean = false;
    public messages: string[];
    public isCreating: boolean = false;

    constructor(private router: Router, private _authService: AuthService){
        console.log(this._authService.getAuth());
        this.model = {username: '', name: '', password: '', repassword: '', address: '', gender: true, role: ''}
    }
    onChangeGender(value: boolean){
        this.model.gender = value;
    }
    onChangeRole(value: string){
        this.model.role = value;
        console.log(this.model.role);
    }
    Login(): void{
        this.isCreating = true;
        this.messages = [];
        this._authService.register(this.model)
        .subscribe(data => {
            this.success = true;
            this.isCreating = false;
            setTimeout(() => {this.router.navigate(['/users'])}, 1000);
        }, err => {
            this.isCreating = false;
            this.messages = err.lstErr;
        });
    }
   
    
}