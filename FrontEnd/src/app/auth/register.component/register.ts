interface IRegister{
    username: string,
    name: string,
    password: string,
    repassword: string,
    address: string,
    gender: boolean,
    role: string

}
export default IRegister;
