 interface IAuthentication{
    isAuth: boolean,
    username: string,
    name: string,
    token: string,
    role: string
}
export interface ITokenResult{
    token: string,
    username: string,
    name: string,
    role: string
}
export default IAuthentication;