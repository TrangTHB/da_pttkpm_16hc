import {IProduct} from '../model/IProduct'

export interface IImport{
    totalMoney: number,
    totalAmount: number,
    import_good_detail: IImportDetail[],
}
export interface IImportDetail{
    price: number,
    product: IProduct,
    amount: number,
}