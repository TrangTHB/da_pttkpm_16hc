import {Component, OnInit} from '@angular/core'
import {IImport as Import, IImportDetail as ImportDetail} from '../../model/IImport'
import {ActivatedRoute, Router} from '@angular/router'
import { Subscription } from 'rxjs/Rx';
import {ImportService} from '../../services/import.service'
import {NotificationService} from '../../services/notification.service';

@Component({
    moduleId: module.id,
    templateUrl: './import-edit.component.html',
    styleUrls: ['./import-edit.component.css'],
    providers: [NotificationService]
})
export class ImportEditComponent implements OnInit{
     public messages: string[];
     public model: Import;
     subscription: Subscription;
     isCreating: boolean = false;
     importID: string;
     constructor(private _route: ActivatedRoute, private _importService: ImportService, private _notiService: NotificationService){

     }
     getDetail(){
         this.model = undefined;
         this._importService.getImportDetail(this.importID).subscribe(_import => {
                this.model = _import;
             }, err => {
                 console.log(err);
             });
     }
     ngOnInit(): void{
        this.subscription = this._route.params.subscribe(params => {
            this.importID = this._route.snapshot.params['importID'];
            this.getDetail();
        });
     }
      editQty(detail: ImportDetail){
        if (detail.amount == null || Number.isInteger(detail.amount) == false){
            detail.amount = 1;
            this._notiService.printSuccessMessage("Số lượng sản phẩm không đúng định dạng !");
        }
        else
        {
            if (detail.amount < 1){
                detail.amount = 1;
                this._notiService.printErrorMessage("Số lượng sản phẩm không đúng định dạng !");
            }
        }
    }
     update(detail: ImportDetail){
         this.isCreating = true;
         this.messages = [];
         this._importService.updateDetail(detail)
         .subscribe(success => {
             console.log(success);
            this.isCreating = false;
            this.getDetail();
            this._notiService.printSuccessMessage('Cập nhật thông tin chi tiết thành công');
            }, err => {
            console.log(err);
            this.isCreating = false;
            this.getDetail();
            this._notiService.printErrorMessage('Cập nhật thông tin chi tiết không thành công');
            this.messages = err.lstErr;
         })

     }
    
}