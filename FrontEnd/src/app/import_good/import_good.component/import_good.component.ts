import {Component} from '@angular/core'
import {ProductService} from '../../services/product.service'
import {IProduct} from '../../model/IProduct'
import {IImport, IImportDetail} from '../importModel'
import {NotificationService} from '../../services/notification.service'
import {ImportService} from '../../services/import.service'
@Component({
    templateUrl: './import_good.component.html',
    styleUrls: ['./import_good.component.css']
})

export class ImportComponent {
    public products: IProduct[];
    public keyword: string;
    public Import: IImport;
    public isCreating: boolean = false;
    public messages: string[] = [];

    constructor(private _productService: ProductService,
                private _notiService: NotificationService,
                private _importService: ImportService){
        this.setInitOrder();
        this.loadProduct();
    }
    loadProduct(){
        this._productService.getListForImport().subscribe(h => {
            this.products = h;
        }, error => {
            console.log(error);
        })
    }
   
    setInitOrder(){
        this.Import = {
            import_good_detail: [],
            totalMoney: 0,
            totalAmount: 0,
        }
        this.calcTotalMoney();
        this.products = undefined;
    }
    addProduct(product: IProduct){
        
        let importDetail: IImportDetail = this.Import.import_good_detail.find(h => h.product._id == product._id);
        if (importDetail){
            
           importDetail.amount++;
           this.calcTotalMoney();
        }
        else{
            importDetail = {
                amount: 1,
                price: product.cost,
                product: product
            };
            this.Import.import_good_detail.push(importDetail);
            this.calcTotalMoney();

        }
    }
    calcTotalMoney(){
        this.Import.totalMoney = 0;
        this.Import.totalAmount = 0;
        this.Import.import_good_detail.forEach(h => {
            this.Import.totalMoney += h.price * h.amount;
            this.Import.totalAmount += h.amount;
        })
        
    }
    remove(detail: IImportDetail){
        let index = this.Import.import_good_detail.findIndex(h => h.product._id == detail.product._id);
        this.Import.import_good_detail.splice(index, 1);
        this.calcTotalMoney();
    }
   
    onSubmit(){
        this.isCreating = true;
        console.log(this.Import);
        this._importService.create(this.Import).subscribe(h => {
            console.log(h);
            this.setInitOrder();
            this.loadProduct();
            this.isCreating = false;
            this._notiService.printSuccessMessage("Thêm đơn hàng nhập thành công");

        }, error => {
            this.messages = error.lstErr;
            console.log(error);
            this.isCreating = false;
        })
        
    }
    editQty(detail: IImportDetail){
        if (detail.amount == null  || Number.isInteger(detail.amount) == false){
            detail.amount = 1;
            this._notiService.printSuccessMessage("Số lượng sản phẩm không đúng định dạng !");
        }
        else
        {
            if (detail.amount < 1){
                detail.amount = 1;
                this._notiService.printSuccessMessage("Số lượng sản phẩm không đúng định dạng !");
            }
        }
        this.calcTotalMoney();
    }
    
}