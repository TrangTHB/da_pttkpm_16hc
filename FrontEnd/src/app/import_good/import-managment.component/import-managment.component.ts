import { Component, OnInit } from '@angular/core';

import {IImport} from '../../model/IImport'
import {ImportService} from '../../services/import.service'
import {NotificationService} from '../../services/notification.service'
declare var jQuery:any;

@Component({
  templateUrl: './import-managment.component.html',
  providers: [NotificationService],
  moduleId: module.id
})

export class ImportManamentComponent  {
    lstImport: IImport[];
    deleteItem: IImport;
    public IsDelete: boolean = false;
    constructor(private _importService: ImportService, private _notiService: NotificationService){};
    handleDeleteDone(rs: boolean){
        this.LoadData();
    }
    LoadData(): void{
         this.lstImport = undefined;
         this._importService.getList()
                .subscribe(
                    data => {
                        this.lstImport = data;
                    },
                    (error: any) => console.log("Lỗi xảy ra ở HTTP service")
                    );
    }
    ngOnInit(): void {
            this.LoadData();
   }
    ngAfterViewChecked(){
       jQuery('#table-list').DataTable();
    }
    delete(item: IImport): void{
           this._notiService.openConfirmationDialog('Bạn có muốn xóa đơn hàng nhập sai này?',
        () => {
            this._importService.delete(item._id)
                .subscribe(() => {
                    this.lstImport.splice(this.lstImport.indexOf(item), 1);
                    this._notiService.printSuccessMessage('Xóa đơn nhập thành công');
                },error =>{
                    if (error.lstErr){
                        this._notiService.printErrorMessage('Xóa không thành công: ' +  error.lstErr.join(', '));
                    }
                    else
                    {
                        this._notiService.printErrorMessage('Xóa không thành công');
                    }
                });

        });
   }
   
   
}
