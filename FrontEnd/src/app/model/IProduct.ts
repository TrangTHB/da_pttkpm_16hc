
export interface IPublisher{
    _id: string,
    name: string
}
export interface ICategory{
    _id: string,
    name: string
}

export interface IProduct{
    _id: string;
    name: string,
    productId: string,
    retail_price: number,
    promotion_price: number,
    cost: number,
    publisher: IPublisher,
    qty: number,
    date_create: Date,
    date_modify: Date,
    category: ICategory[],
    isdelete: boolean, 
    img_main: string
}