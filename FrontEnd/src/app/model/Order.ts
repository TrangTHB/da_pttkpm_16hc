import {User} from "./user";
import {OrderDetail} from "./OrderDetail";
import {Product} from "./Product";
import {Status} from "./Status";



export class Order{



//  public static convertToOrders(data: any): Order[]{

//         let result : Order[] = [];
//         for(let order of data){
          
//            let user = new User(order.user._id, order.user.name);
    
//            //Order details
//            let orderDetails = [];
//            for(let detail of order.orderDetail){
//                let product : Product = new Product(detail.product._id, detail.product.name);
//                console.log(product);

//                let order: OrderDetail = new OrderDetail(detail._id,product);
//                orderDetails.push(order);
//            }

//            let orderResult: Order = new Order(order._id,order.totalMoney,order.createdAt,user,orderDetails);
//            result.push(orderResult);

//         }
//         return result;
//     }


    constructor(public _id:string,public totalMoney:number, public createdAt : Date,public statement: Status,
        public user: User, public orderDetail : OrderDetail[]){}

}