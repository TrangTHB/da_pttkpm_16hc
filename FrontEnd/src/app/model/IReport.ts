export interface IYearReport{
    year: number,
    months: IMonthReport[]
}
export interface IMonthReport{
    month: number,
    totalAmountOrder: number,
    totalMoneyOrder: number,
    totalAmountImportGood: number,
    totalMoneyImportGood: number,
    profit: number
}