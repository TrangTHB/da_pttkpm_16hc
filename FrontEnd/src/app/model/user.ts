import {IRole} from './role'
export interface IUser{
    _id: string,
    updateAt: Date,
    createdAt: Date,
    username: string,
    gender: boolean,
    name: string,
    isdelete: boolean,
    address: string,
    role: IRole,
    img: string
}
export class User{    
    constructor(private _id: string, private name: string){
        
    }
}