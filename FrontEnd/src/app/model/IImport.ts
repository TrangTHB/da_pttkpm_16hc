import {IProduct} from './IProduct'

export interface IImport{
    _id: string,
    totalMoney: number,
    totalAmount: number,
    import_good_detail: IImportDetail[],
}
export interface IImportDetail{
    _id: string,
    order: string,
    price: number,
    product: IProduct,
    amount: number,
}