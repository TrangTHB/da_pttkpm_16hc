export interface IDelivery{
    _id: string,
    customer_name: string,
    phone: string,
    address: string,
    delivery_date: Date,
    cost: number,
    note: string,
}