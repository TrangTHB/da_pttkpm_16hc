import {Component, OnInit} from '@angular/core'
import {AuthService} from '../../auth/auth.service'
import IAuthencation from '../../auth/authentication'
import {Router} from '@angular/router'
import {UserService} from '../../user/user.service'
import {IUser} from '../../model/user'
@Component({
    selector: '[login-info-nav]',
    templateUrl: './login-info.component.html',
    styleUrls: ['./login-info.component.css']
})
export class LoginInfoComponent implements OnInit{
    public auth: IAuthencation;
    public user: IUser;
    constructor(private _authService: AuthService
            , private _router: Router
            , private _userService : UserService){
        this.auth = this._authService.getAuth();
    }
    ngOnInit(){
        if (this._authService.auth.isAuth == true){
            this._userService.getNewInfo();
        }
         this.user = this._userService.user;
    }
    
    logOut(): void{
        this._authService.logOut();
        this._userService.clearUserInfo();
        this._router.navigate(['/home']);
    }
}