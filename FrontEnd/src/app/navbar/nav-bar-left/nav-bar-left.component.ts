import { Component } from '@angular/core';
import { AuthService } from '../../auth/auth.service'
import IAuthentication from '../../auth/authentication'
@Component({
    selector: 'nav-bar-left',
    templateUrl: './nav-bar-left.component.html'
})
export class NavBarLeftComponent {
    public user: any;
    public auth: IAuthentication;
    constructor(private _authService: AuthService){

        this.auth = this._authService.getAuth();
    }
}
