import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, RequestOptions, XHRBackend  } from '@angular/http';
import { RouterModule, Router} from '@angular/router'
import { AppComponent } from './app.component';
import {AppRoutingModule} from './app.routing.module';

import {LoginInfoComponent} from './navbar/login-info/login-info.component';
import {NavBarLeftComponent} from './navbar/nav-bar-left/nav-bar-left.component';
import {NavBarTopComponent} from './navbar/nav-bar-top/nav-bar-top.component';
import {HomeComponent} from './home/home.component';

import {CategoryComponent} from './pages/category.component/category.component';
import {AddCategoryComponent} from './pages/category.component/addCategory.componemt';
import {ModifyCategoryComponent} from './pages/category.component/modifyCategory.compoment';


import {PublisherComponent} from './pages/publisher.componemt/index.compoment';
import {AddPublisherComponent} from './pages/publisher.componemt/addPublisher.componemt';
import {ModifyPublisherComponent} from './pages/publisher.componemt/modifyPublisher.compoment';

import { ModalModule } from 'ng2-bootstrap/ng2-bootstrap';
//Order
import {OrderComponent} from './pages/orderManager.componemt/index.component';


//Shared
import {LoadingComponent} from './shared/loading.component/loading.component'
import {RoleComboboxComponent} from './shared/role.combobox.component/role.combobox.component'
//Auth
import {LoginComponent} from './auth/login.component/login.component'
import {ProfileComponent} from './auth/profile.component/profile.component'
import {RegisterComponent} from './auth/register.component/register.component'
import {UnAuthenGuard, AuthenGuard, AuthenSellerGuard, AuthenManagerGuard, AuthenAdminGuard} from './auth/guards/authen-guard'
import {AuthService} from './auth/auth.service'
import {UserService} from './user/user.service'
import {HttpInterceptor, LoadHttp} from './http.interceptor'
//User
import {UserListComponent} from './pages/user.component/user-list.component/user-list.component'
import {EditUserComponent}  from './pages/user.component/edit-user.component/edit-user.component'

//Product
import {ProductListComponent} from './pages/product.component/product-list.component/product-list.component'
import {ProductService} from './services/product.service'
import {ProductCreateComponent}  from './pages/product.component/product-create.component/product-create.component'
import {PublisherService} from './services/service_Publisher'
import {CatService} from './services/service_cat';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import {ProductEditComponent} from './pages/product.component/product-edit.component/product-edit.component'
//Sale

import {ProductPipe} from './pipes/productFilter'
import {NotificationService} from './services/notification.service'
import {SaleComponent} from './sale/sale.component/sale.component'
import {CostService} from './services/cost.service'
import {OrderService} from './services/service_order'

//Import 
import {ImportComponent} from './import_good/import_good.component/import_good.component'
import {ImportService} from './services/import.service'
import {ImportManamentComponent} from './import_good/import-managment.component/import-managment.component'
import {ImportEditComponent} from './import_good/import-edit.component/import-edit.component'

//Cost
import {CostListComponent} from './pages/cost.component/cost-list.component/cost-list.component'
import {CostEditComponent} from './pages/cost.component/cost-edit.component/cost-edit.component'
import {CostAddComponent} from './pages/cost.component/cost-add.component/cost-add.component'
//Report
import {ReportService} from './services/report.service'
import {ReportGeneralComponent} from './report/report-general.component/report-general.component'

import {DeliverySerivce} from './services/delivery.service'
import {DeliveryManagementComponent}  from './pages/delivery.component/delivery.management.component/delivery.management.component'
import {DeliveryEditComponent} from './pages/delivery.component/delivery-edit.component/delivery-edit.component'
@NgModule({
  declarations: [
    AppComponent,
    LoginInfoComponent,
    NavBarLeftComponent,
    NavBarTopComponent,
    HomeComponent,
    CategoryComponent,
    AddCategoryComponent,
    ModifyCategoryComponent,

    //Publisher
    PublisherComponent,
    AddPublisherComponent,
    ModifyPublisherComponent,
    LoadingComponent,
    LoginComponent,
    ProfileComponent,
    RegisterComponent,
    RoleComboboxComponent,
    UserListComponent,
    EditUserComponent,
    //Order
    OrderComponent,
    //Prouduct,
    ProductListComponent,
    ProductCreateComponent,
    ProductEditComponent,
    SaleComponent,
    ProductPipe,
    ImportComponent,
    CostListComponent,
    CostEditComponent,
    CostAddComponent,
    ImportManamentComponent,
    ImportEditComponent,
    ReportGeneralComponent,
    DeliveryManagementComponent,
    DeliveryEditComponent
    


  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,  
    AppRoutingModule,
    ModalModule.forRoot(),
    MultiselectDropdownModule,
  ],

  
  providers: [AuthService, UnAuthenGuard, AuthenGuard, AuthenSellerGuard, AuthenManagerGuard, AuthenAdminGuard,
   UserService, ProductService, PublisherService, CatService,
   ProductPipe, NotificationService, CostService, OrderService, ImportService, ReportService,
   DeliverySerivce,
   {
      provide: HttpInterceptor,
      useFactory: LoadHttp,
      deps: [XHRBackend, RequestOptions, Router, AuthService]
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
