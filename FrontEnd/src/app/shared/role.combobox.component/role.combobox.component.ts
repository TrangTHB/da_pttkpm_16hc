import {Component, OnInit, Output, Input, EventEmitter} from '@angular/core'
import {UserService} from '../../user/user.service'
import {IRole} from '../../model/role'
@Component({
    selector: 'role-combobox',
    styleUrls: ['./role.combobox.component.css'],
    templateUrl: './role.combobox.component.html'
})
export class RoleComboboxComponent implements OnInit{
    public lstRoles: IRole[] = [];
    @Output() eventChange = new EventEmitter();
    @Input() selectValue: string;
    constructor(private _userService: UserService){
        this._userService.getRoles().subscribe(data => {
            this.lstRoles = data;
        }, err => {
            console.log(err);
        })
    }
    ngOnInit(){
       if (!this.selectValue){
           this.selectValue = "-1";
       }
    }
    onChange(value){
        console.log(this.selectValue);
        if (value != -1) this.eventChange.emit(value);
    }
    ngOnChanges() {

    }
    console(){
        console.log(this.selectValue);
    }
    
}