var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
ObjectId = require('mongodb').ObjectID;

var common = require('../common.js');

var deliveryModel = new Schema({
    customer_name :  {   type: String ,required: [true, 'Tên khách hàng không được bỏ trống']},
	order : {type: Schema.Types.ObjectId, ref: 'Order'},
    phone : {   type: String ,required: [true, 'Số điện thoại không được bỏ trống']},
    address : {   type: String ,required: [true, 'Địa chỉ không được bỏ trống']},
    cost: 
    {
            type: Number ,
            required: [true, 'Phí vận chuyển không được để trống'], 
            set: common.setNumberOrUndefined,
            validate: [common.isNumberOrEmpty, 'Phí vận chuyển phải là kiểu số'],
    },     
    note : {   type: String },
    isdelete:{type: Boolean, default:false}
},
{timestamps: true});

module.exports = mongoose.model('Delivery', deliveryModel);
