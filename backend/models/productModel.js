var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

    
var productModel = new Schema({
	retail_price: {type: Number},
	name: {type: String, trim: true},
	productId: {type: String},
	promotion_price: {type: Number},
	cost: {type: Number},
	category: [{type: Schema.Types.ObjectId, ref: 'Category'}], 
	publisher: {type: Schema.Types.ObjectId, ref: 'Publisher'},
	img_main: {type: String},
	qty: {type: Number},
	//date_create: {type: Date},
	//date_modify: {type: Date},
	isdelete: {type: Boolean, default: false}
},
{timestamps: true});

module.exports = mongoose.model('Product', productModel);