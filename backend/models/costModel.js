var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var common = require('../common.js');
    
var costModel = new Schema({
    name: { type : String , required: [true, 'Tên quận không được bỏ trống']},
    price: 
    {
            type: Number ,
            required: [true, 'Giá tiền không được để trống'], 
            set: common.setNumberOrUndefined,
            validate: [common.isNumberOrEmpty, 'Giá tiền phải là kiểu số'],
    }, 
    isdelete: {type: Boolean,"default": false}
}, 
{timestamps: true});

module.exports = mongoose.model('Cost', costModel);