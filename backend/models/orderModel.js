var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
ObjectId = require('mongodb').ObjectID;

var common = require('../common.js');

var orderModel = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User'},
    
    totalMoney: 
    {
            type: Number ,
            required: [true, 'Tổng tiền không được để trống'], 
            set: common.setNumberOrUndefined,
            validate: [common.isNumberOrEmpty, 'Tổng tiền phải là kiểu số'],
    },  
    totalAmount: 
    {
            type: Number ,
            required: [true, 'Tổng sô lượng không được để trống'], 
            set: common.setNumberOrUndefined,
            validate: [common.isNumberOrEmpty, 'Tổng số lượng phải là kiểu số'],
    },                 
    statement: {type: Schema.Types.ObjectId, ref: 'Statement', required: [true, 'Trạng thái đơn hàng không được để trống']},
    //orderDetail: { type: [Schema.Types.ObjectId], ref: 'Orderdetail', required: [true, 'Chi tiết đơn hàng không được để trống']},
    typePayment:
    {
            type: Number ,
            required: [true, 'Loại thanh toán không được để trống'], 
            set: common.setNumberOrUndefined,
            validate: [common.isNumberOrEmpty, 'Loại thanh toán phải là kiểu số'],
    },  
    orderDetail:  [{ type: Schema.Types.ObjectId, ref: 'Orderdetail' }],
    isdelete:{type: Boolean, default:false}
},
{timestamps: true});

module.exports = mongoose.model('Order', orderModel);