var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var common = require('../common.js');

var import_good_detailModel = new Schema({
    idImport: {type: Schema.Types.ObjectId, ref: 'Import_good' },
    product:  { type: Schema.Types.ObjectId, ref: 'Product', required: [true, 'Sản phẩm không được để trống']},
    price: 
    {
        type: Number,  
        required: [true, 'Giá sản phẩm không được để trống'] ,
        set: common.setNumberOrUndefined,
        validate: [common.isNumberOrEmpty, 'Giá nhập phải là kiểu số'],
    },
    amount: 
    {
        type: Number,  
        required: [true, 'Số lượng không được để trống'] ,
        set: common.setNumberOrUndefined,
        validate: [common.isNumberOrEmpty, 'Số lượng nhập phải là kiểu số'],
    },
});

module.exports = mongoose.model('Import_good_detail', import_good_detailModel);