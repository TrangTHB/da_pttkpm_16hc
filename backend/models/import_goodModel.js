var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var common = require('../common.js');
    
var import_goodModel = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User'},
    totalMoney: 
    {
            type: Number ,
            required: [true, 'Tổng tiền không được để trống'], 
            set: common.setNumberOrUndefined,
            validate: [common.isNumberOrEmpty, 'Tổng tiền phải là kiểu số'],
    },
    totalAmount: 
    {
            type: Number ,
            required: [true, 'Tổng sô lượng không được để trống'], 
            set: common.setNumberOrUndefined,
            validate: [common.isNumberOrEmpty, 'Tổng số lượng phải là kiểu số'],
    },   
    isdelete:{type: Boolean, default:false}
}, 
{timestamps: true});

module.exports = mongoose.model('Import_good', import_goodModel);