 //FUNCTION NGHEP VU
var productBus = function(Product){

    var getItem  = function(id){
        //Promise run all success
        var promise = new Promise(function(resolve, reject) {
            Product.findById(id, function(err, product)
            {
                if(err){
                        reject(err);
                }
                else
                {
                    resolve(product);
                }
            
            });
            
        });
        // var productBus = require('../bus/productBus')(Product);
        // var productPrommise  =productBus.getItem('58ea08c5bab96280ae7eb1bb');
        // productPrommise.then(function(product){

        //     console.log(product);
        // })
        return promise;
    }
    

     return {
        //post: post,
       // get:get,
        getItem: getItem,
       // patch: patch,
       // delete : deleted,
    }
}

module.exports = productBus;