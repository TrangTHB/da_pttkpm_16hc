var async    = require('async'),
    mongoose = require('mongoose'),
    cloudinary = require('cloudinary');

cloudinary.config({
	cloud_name: 'dxnapa5zf',
	api_key: '219348637198157',
	api_secret: 'FJl9rCE5dTS_kgkX_rPvUyMTwZY'
});
function bodauTiengViet(str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.toUpperCase();
        return str;
}
var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};
var productController = function(Product, ImportGoodDetail, OrderDetail){

    var post = function(req, res){
        let model = JSON.parse(req.body.model);
        let lstErr = [];
        if (!model.name)
        {
            lstErr.push('Tên không được để trống.');
        }
		else if(model.name == '')
		{
			lstErr.push('Tên không được để trống.');
		}
		if(model.cost <= 0)
		{
			lstErr.push('Giá nhập không được nhỏ hơn hoặc bằng 0.');
		}
		if(model.retail_price <= 0)
		{
			lstErr.push('Giá bán không được nhỏ hơn hoặc bằng 0.');
		}
		if(model.promotion_price < 0)
		{
			lstErr.push('Giá khuyến mãi không được nhỏ hơn 0.');
		}
		if(model.cost > model.retail_price)
		{
			lstErr.push('Giá nhập không được lớn hơn giá bán.');
		}
        if (!model.publisher || model.publisher == "-1")
        {
            lstErr.push('Phải chọn nhà sản xuất.');
        }
        if (!model.category || model.category.length === 0){
            lstErr.push('Phải chọn loại sản phẩm.');
        }
		if(lstErr.length > 0)
		{
			res.status(404).json({lstErr: lstErr});
		}
        else
        {
			
			if (model.promotion_price == 0)
			{
				model.promotion_price = model.retail_price;
			}
			
            async.waterfall([
				function(callback){
                    if (req.file){
                        cloudinary.uploader.upload(req.file.path, function(result){
                           
                            callback(null, result.url);
                        });
                    }
                    else {
						
						callback(null, 'http://res.cloudinary.com/dxnapa5zf/image/upload/v1495285100/download_pkoh8x.jpg');
					} 
                },
                 function(url, callback){
                    let productId = '';
                    bodauTiengViet(model.name).split(' ').forEach(function(value, index){
                        productId += value[0];
                    });
                    let now = new Date();
                    let strTime = now.getTime().toString().split("").reverse().join("");
                    if (productId.length > 5){
                        productId.substring(0, 5);
                    }
                    productId += strTime.substring(0, 10 - productId.length);
                    callback(null,url, productId);
                },
                function(url, productId, callback){
                    let p = new Product({
                        name: model.name,
                        retail_price : model.retail_price,
                        promotion_price : model.promotion_price,
                        cost : model.cost,
                        category : model.category,
                        publisher : model.publisher,
                        productId: productId,
						img_main: url,
                        qty : 0
                    });
                    callback(null, p);

                },
                function(p, callback){
                    if (req.file){
                        cloudinary.uploader.upload(req.file.path, function(result){
                            p.img_main = result.url;
                            callback(null, p);
                        });
                    }
                    else callback(null, p);
                }
                
            ], function(err, p){
                if (err){
                     lstErr.push('Có lỗi trong quá trình xử lý');
                     res.status(404).json({lstErr: lstErr});
                }
                else
                {
                    p.save(function(err){
                        if (err){
                            console.log(err);
                             lstErr.push('Có lỗi trong quá trình xử lý');
                             res.status(404).json({lstErr: lstErr});
                        }
                        else{
                            res.json(p);
                        }
                    })
                    
                }
            })
        }
    }
	
    var get = function(req,res){
		res.setHeader('Access-Control-Allow-Origin', '*');
		var query = {};
		query.isdelete = false;
        if (req.query.qty){
            query.qty = {$gte: Number(req.query.qty)}
        }
		Product.find(query)
		.populate([{
			path: 'category',
			select: ['name']
		},
		{
            path: 'publisher',
            select: ['name']
        }
		])
		.exec(function(err, products)
        {
            if (err){
                  sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }
            else  {
                  sendJsonResponse(res,200,products);
            }
        });
    }   
    var getItem = function(req,res){   
        res.json(req.product.toJSON());
		
    }
    var patch = function(req,res){
        //if(req.body._id)
            //delete req.body._id;

		let model1 = JSON.parse(req.body.model);
        let lstErr1 = [];
        if (!model1.name)
        {
            lstErr1.push('Tên không được để trống.');
        }
		else if(model1.name == '')
		{
			lstErr1.push('Tên không được để trống.');
		}
		if(model1.cost <= 0)
		{
			lstErr1.push('Giá nhập không được nhỏ hơn hoặc bằng 0.');
		}
		if(model1.retail_price <= 0)
		{
			lstErr1.push('Giá bán không được nhỏ hơn hoặc bằng 0.');
		}
		if(model1.promotion_price < 0)
		{
			lstErr1.push('Giá khuyến mãi không được nhỏ hơn 0.');
		}
		if(model1.cost > model1.retail_price)
		{
			lstErr1.push('Giá nhập không được lớn hơn giá bán.');
		}
        if (!model1.publisher || model1.publisher == "-1")
        {
            lstErr1.push('Phải chọn nhà sản xuất.');
        }
        if (!model1.category || model1.category.length === 0){
            lstErr1.push('Phải chọn loại sản phẩm.');
        }
		if(lstErr1.length > 0)
		{
			res.status(404).json({lstErr: lstErr1});
		}
        else
        {
			if (model1.promotion_price == 0)
			{
				model1.promotion_price = model1.retail_price;
			}
            async.waterfall([
                function(callback){
					if (req.file){
                        cloudinary.uploader.upload(req.file.path, function(result){
                           callback(null, result.url);
                        });
                    }
                    else {
						callback(null, '');
					}
                },
                function(src, callback){
                    let pro1 = req.product;
                    pro1.name = model1.name;
                    pro1.retail_price = model1.retail_price;
                    pro1.promotion_price = model1.promotion_price;
                    pro1.cost = model1.cost;
					pro1.category = model1.category;
					pro1.publisher = model1.publisher;
                    if (src != ''){
                        pro1.img_main = src;
                    }
					//pro.qty = model.qty;
                    callback(null, pro1);
                
                },
                
            ], function(err, pro1){
                if (err){
                     lstErr1.push('Có lỗi trong quá trình xử lý');
                     res.status(404).json({lstErr: lstErr1});
                }
                else
                {
                    pro1.save(function(err){
                        if (err){
                            console.log(err);
                             lstErr1.push('Có lỗi trong quá trình xử lý');
                             res.status(404).json({lstErr: lstErr1});
                        }
                        else{
                            res.json(pro1);
                        }
                    })
                    
                }
            })
        }
    }
	
     var deleted = function(req,res){
	   
	   //xet xem so luong co bang 0
	   //co trong bang ban hang
	   //co trong bang nhap hang
	   //=> khong cho xoa

	   async.waterfall([
		function(callback){
			ImportGoodDetail.find({product :  new mongoose.mongo.ObjectID(req.params.productID)})
			.exec(function(err, import_good)
			{
				if(err){
					console.log(err);
                    callback('Có lỗi trong quá trình xử lý', null);
                }
                else if(import_good && import_good.length)
                {
                    callback('Sản phẩm đã tồn tại trong 1 hoặc nhiều đơn nhập', null)
                }
                else callback(null, 1)
			});
		},function(number, callback){
			OrderDetail.find({product :  new mongoose.mongo.ObjectID(req.params.productID)})
			.exec(function(err, order)
			{
				if(err){
					console.log(err);
                    callback('Có lỗi trong quá trình xử lý', null);
                }
                else if(order && order.length)
                {
                    callback('Sản phẩm đã tồn tại trong 1 hoặc nhiều đơn bán hàng', null)		
                }
                else callback(null, 1)

			});
		}
	   ],function(err, result){
			{
                if (err){
                    res.status(404).json({lstErr: [err]})
                }
                else
                {
                    if (req.product.qty != 0)
                    {
                        res.status(404).json({lstErr: 'Số lượng sản phẩm phải bằng 0 mới được xóa'})
                    }
                    else
                    {
                        req.product.isdelete = true;
                        req.product.save(function(err){
                            if(err)
                            {
                                res.status(404).send(err);
                            }
                            else{
                                sendJsonResponse(res,200);
                            }
                        });
                    }
                }
				
			}
			
		})
     };
    return {
        post: post,
        get:get,
        getItem: getItem,
        patch: patch,
        delete : deleted,
    }
}
module.exports = productController;