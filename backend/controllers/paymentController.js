var async    = require('async'),
    mongoose = require('mongoose');

var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};
var paymentController = function(Order, OrderDetail){

    var post = function(req, res){

        // Kiểm tra authen

        if (!req.auth 
            || (req.auth.role != 'seller'
             && req.auth.role != 'manager' 
             && req.auth.role != 'admin')){ 
                req.status(403).send('Authorization is required');
        }
        else
        {
            let currentUser = req.auth.username;
            var reqOrder = new Order(req.body);
            var reqOderDetail = new OrderDetail(req.body);
            var errorOrder = reqOrder.validateSync();
            var errorOderDetail = reqOderDetail.validateSync();
            //gan temp user co dinh. Khi login thi thy user nay lai
            reqOrder.user =  currentUser;
        
            var lstErr = [];
            var lstFieldErr = [];
            //Check error TH Order
            if (errorOrder) {
                if (errorOrder.name == 'ValidationError') {
                
                    for (field in errorOrder.errors) {
                    lstFieldErr.push(field);
                    lstErr.push(errorOrder.errors[field].message);
                    }
                }
            }
            //Check error TH OrderDetail
            if (errorOderDetail) {
                if (errorOderDetail.name == 'ValidationError') {
                
                    for (field in errorOderDetail.errors) {
                        lstFieldErr.push(field);
                        lstErr.push(errorOderDetail.errors[field].message);
                    }
                }
            }
            if (lstErr.length){
                console.log(lstErr);
                sendJsonResponse(res,404,{	lstErr:lstErr ,lstFieldErr: lstFieldErr});	
            }
            else{
            
                //Tam thoi do product la 1 list , chua tinh th nay. temp get 2 san pham
                //Khi co du thieu thuc su thi for de duyet vao bien nay.
                //Boi vi chua bik layout giao dien xe nhu the nao. Nen chua bik
                //cach thuc du lieu xe duoc luu nhu the nao
                //Chay du lieu ao thui
                    var OrderDetailsFromUser  = [
                        {
                        product: reqOderDetail.product,
                        amount: reqOderDetail.amount,
                        price: reqOderDetail.price,
                        //price: 'abc',
                },
                        {
                        product: reqOderDetail.product,
                        amount: reqOderDetail.amount,
                        price: reqOderDetail.price,
                        } 
                    ];
                        
                var orderdetails = OrderDetailsFromUser.map(function(od){
                    return new OrderDetail(
                    {
                        _id:  ObjectId(new ObjectId()),
                        order: reqOrder._id,
                        product: od.product,
                        amount: od.amount,
                        price: od.price 
                    });
                });
                reqOrder.orderDetail =  orderdetails.map(function(od){
                        return od._id;
                })
                var docs = [];
                var check = 0;
                async.waterfall([

                    function(callback){
                        //Iterate over each order detail
                        var check = 0;
                        async.each(orderdetails,function(od, callback){
                                od.save(function(err){
                                if(err){
                                    //Khong return callback o day vi nieu return o day thi no xe tra ve docs = null.
                                    //Do item o gia tri loi no chay truoc gia tri kia
                                //return callback(err, docs);  
                                //Boi vi no chay vong lap bat dong bo, khong theo thu tu trong ds them nen
                                // chung ta phai su dung
                                //Check de luu lai nhung docs da duoc insert success trong qua trinh
                                check =1;
                                }else{
                                    docs.push( od);
                                }
                                callback(null, docs);
                                
                            });
                        },
                        function(err) {
                            if( err ) {
                            //Do doan vong lap da mac dinh no la khong co errror .
                            //TH minh check no la error khi bien check = 1
                            //Khong xu ly cho nay chi can ktra check
                            } else {
                                if(check == 1){
                                    console.log('A order detail failed to process');
                                    return callback('loi',docs);

                                } else{
                                    console.log('All order detail have been processed successfully');
                                    callback(null, docs);
                                }
                            }
                        })},
                function(detail , callback){
                    //VD Test for roolback
                        //reqOrder.totalMoney = 'abc';
                        reqOrder.save(function(err){ 
                            if (err){
                                    console.log('A order failed to process');
                                    return callback(err, docs);
                            }
                            console.log('Order have been processed successfully');
                            docs.push(reqOrder);
                            callback(null, docs);
                        })

                },  
            ],
            function (errs, results) {
                if (errs) {
                    console.log(results);
                    async.each(results, rollback, function () {
                        console.log('Rollback done.');
                    });
                    sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
                
                } else {
                    console.log('Done.');
                    sendJsonResponse(res,200,results);
                }
            });

            function rollback (doc, callback) {
            if (doc == null) {
                callback();
            }
            else {
                OrderDetail.findByIdAndRemove({_id :  new mongoose.mongo.ObjectID(doc._id)}, function (err, doc) {  
                    if(err)
                    {
                        console.log('Error Rolled-back document: ' + err);
                    }
                    console.log('Rolled-back document: ' + doc._id);
                    callback();
                });
                }
            }
        }
        }

        
    }
    
    return {
        post: post
    }
}
module.exports = paymentController;
