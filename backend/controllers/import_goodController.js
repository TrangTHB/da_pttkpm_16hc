var async    = require('async'),
    mongoose = require('mongoose');

var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};

var import_goodController = function(ImportGood, ImportGoodDetail, Product){

    var post = function(req, res){
        if (!req.auth 
            || (req.auth.role != 'seller'
             && req.auth.role != 'manager' 
             && req.auth.role != 'admin')){ 
                req.status(403).send('Authorization is required');
        }
        else
        {
            let currentUser = req.auth.userID;
            var data = req.body;
            var reqGood = new ImportGood({
                    user:  currentUser,
                    totalMoney: data.totalMoney,
                    totalAmount: data.totalAmount
                });
                var errorGood = reqGood.validateSync();

                var lstErr = [];
                var lstFieldErr = [];
                //Check error TH Good
                if (errorGood) {
                    if (errorGood.name == 'ValidationError') {
                    
                        for (field in errorGood.errors) {
                        lstFieldErr.push(field);
                        lstErr.push(errorGood.errors[field].message);
                        }
                    }
                }
                var Gooddetails = data.import_good_detail.map(function(od){
                
                    var reqGoodDetail = new ImportGoodDetail({
                        idImport: reqGood._id,
                        amount: od.amount,
                        price: od.price,
                        product: od.product._id,
                    });
                    var errorGoodDetail = reqGoodDetail.validateSync();
                        if (errorGoodDetail) {
                        if (errorGoodDetail.name == 'ValidationError') {
                        
                            for (field in errorGoodDetail.errors) {
                                lstFieldErr.push(field+ "_"+ od.product.productId);
                                lstErr.push(errorGoodDetail.errors[field].message + " tại sản phẩm "+ od.product.productId);
                            }
                        }
                        }
                    return reqGoodDetail;
                });
            
                if (lstErr.length){
                    console.log(lstErr);
                    sendJsonResponse(res,404,{	lstErr:lstErr ,lstFieldErr: lstFieldErr});	
                }
                else{
                    
                    var docs = [];
                    var check = 0;
                    async.waterfall([
                        
                        function(callback){
                            //Iterate over each Good detail
                            var check = 0;
                            async.each(Gooddetails,function(od, callback){
                                    od.save(function(err){
                                    if(err){
                                        //Khong return callback o day vi nieu return o day thi no xe tra ve docs = null.
                                        //Muc dich la de get data de su dung cho viec rollback
                                    check =1;
                                    }else{
                                        docs.push( od);
                                    }
                                    callback(null, docs);
                                    
                                });
                            },
                            function(err) {
                                if( err ) {
                                //Do define doan vong lap da mac dinh no la khong co errror .
                                //Khong check cho nay 
                                } else {
                                    if(check == 1){
                                        console.log('A Good detail failed to process');
                                        return callback('loi',docs);

                                    } else{
                                        console.log('All Good detail have been processed successfully');
                                        callback(null, docs);
                                    }
                                }
                            })},
                            //Luu hoa don nhap
                        function(docs , callback){
                            reqGood.save(function(err){ 
                                if (err){
                                        console.log('A Good failed to process');
                                        return callback(err, docs);
                                }
                                console.log('Good have been processed successfully');
                                callback(null, docs);
                            })
                    },  
                ],
                function (errs, results) {
                    if (errs) {
                        async.each(results, rollback, function () {
                            console.log('Rollback done.');
                        });
                        sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
                    
                    } else {
                        //Khi qua trinh insert hoan tat--> tien hanh update so luong ton tron bang product
                            var tasks = results.map(function(item){
                            return function(callback)
                            {
                                Product.findById(item.product, function(err,product){
                                    if(err){
                                        console.log(err);
                                        callback(error);
                                    }
                                    else
                                    {
                                        var qtyTotal =  item.amount +  product.qty;
                                        var updateObj = {
                                            qty: qtyTotal
                                        };        
                                        Product.findByIdAndUpdate(item.product, updateObj, function(err, model) {
                                            if (err){
                                                callback(error); 
                                            }
                                            else callback(null, model);
                                        });
                                    }
                                });
                            }
                        });
                        async.parallel(tasks, function(err, result){
                            if (err){ //Nếu bất kì 1 task nào trong list bị lỗi rơi vào đây
                                console.log(err);
                                sendJsonResponse(res,404,err); 
                            }
                            else {
                                console.log("Done");
                                sendJsonResponse(res,200,result); 
                            //Result lúc này là mảng các Product truoc do chua update
                            }
                        });
                    }
                });

                function rollback (doc, callback) {
                if (doc == null) {
                    callback();
                }
                else {
                    ImportGoodDetail.findByIdAndRemove({_id :  new mongoose.mongo.ObjectID(doc._id)}, function (err, doc) {  
                        if(err)
                        {
                            console.log('Error Rolled-back document: ' + err);
                        }
                        console.log('Rolled-back document: ' + doc._id);
                        callback();
                    });
                    }
                }
            }
        }
       
    }
   
    var get = function(req,res){
        res.setHeader('Access-Control-Allow-Origin', '*');
        ImportGood.find({isdelete: false})
        .populate([{
            path: 'user',
            select: ['name', "address"]
        },
        ])
        .exec(function(err, data)
        {
            if (err){
                  sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }
            else  {
                  sendJsonResponse(res,200,data);
            }
        });
    }   
    //get direct item detail of id import good in controoler import_good
   var getItem = function(req,res){  

        let importId = req.params.import_goodId;
        
        async.waterfall([
            function(callback){
                ImportGood.findById(importId)
                .populate([{
                    path: 'user',
                    select: ['_id', 'username', 'name']
                }])
                .exec(function(error, _import){
                    if (error || !_import){
                        callback('error', null);
                    }
                    else callback(null, _import);
                })
            },
            function(_import, callback){
                  ImportGoodDetail.find({idImport :  new mongoose.mongo.ObjectID(req.params.import_goodId)})
                    .populate([
                    {
                        path: "product",
                        select: ['name', 'img_main']
                    }
                    ])
                    .exec(function(err, importDetails){
                        if (err){
                            callback('error', null);
                        }
                        else
                        {
                            _import._doc.import_good_detail = importDetails;
                            callback(null,_import);
                        }
                    });
             }
             ], function(error, _import){
                        if (error){
                             sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
                        }
                        else res.json(_import._doc);

             });
        
    }
    //Update amount product import_good
    var patch = function(req,res){

        var data =req.body;
        
       var reqImportDetail = new ImportGoodDetail(
        {
            idImport: data.idImport,
            product: data.product._id,
            price: data.price,
            amount: data.amount
        }
       );
        
        var lstErr = [];
        var lstFieldErr = [];
        var error = reqImportDetail.validateSync();

         //Check error 
        if (error) {
            if (error.name == 'ValidationError') {
               
                for (field in error.errors) {
                   lstFieldErr.push(field);
                   lstErr.push(error.errors[field].message);
                }
            }
        }
        if (lstErr.length){
            console.log(lstErr);
            sendJsonResponse(res,404,{	lstErr:lstErr ,lstFieldErr: lstFieldErr});	
        }else{

        var updateObj = {
            amount: req.body.amount
        };
        async.waterfall([
            //get info import_good 
            function(callback){
                ImportGood.findById(reqImportDetail.idImport, function(err,import_good){
                if(err){
                    callback(err);
                }
                else if(import_good){
                    callback(null, import_good)
                }else{
                    //Không  tìm thấy mã chi tiết đơn hàng nhập
                    return callback('error');
                }
                })
             },
             //get info import_good_detail
            function(import_good, callback){
                  ImportGoodDetail.findById(req.params.IdDetail, function(err, dataDetail) {
                    if (err){
                        console.log(err);
                        return callback(err);
                    }else if(dataDetail){
                       callback(null,import_good, dataDetail);
                    }else{
                        //Không  tìm thấy mã chi tiết đơn hàng nhập
                        return callback('error');
                    }
                  })  
            },
             //Update qty product
            function(import_good, dataDetail,callback){
               
                Product.findById(reqImportDetail.product, function(err,product){
                    if (err){
                        console.log(err);
                        return callback(err);
                    }else if(product){
                         var qtypre = dataDetail.amount;
                         var qtyNew = reqImportDetail.amount;
                        var qtyTotalCurrent = qtyNew -  qtypre;
                        var qtyUpdate = product.qty + ( qtyTotalCurrent);
                        if(qtyUpdate <0){
                             lstErr.push('Số lượng tồn của sản phẩm '+ product.productId + " là "+ product.qty + " không đủ cung cấp để cập nhật đơn nhập hàng");
                             callback(null);
                        }else{
                            product.qty= qtyUpdate;
                            product.save(function(err){
                            if(err){
                                console.log(err);
                                return callback(err);
                            }
                            else{
                                //Update table Import_good with column totalAmount and totalMoney.
                                var amountPre =dataDetail.amount ;
                                var amountUpdate = reqImportDetail.amount;
                                if(amountPre != amountUpdate){
                                    var totalAmountUpdate = import_good.totalAmount + (amountUpdate - amountPre);
                                    var totalMoneyUpdate  =  import_good.totalMoney +   (dataDetail.price  * ((amountUpdate - amountPre)));
                                    var updateImportObj = {
                                        totalMoney: totalMoneyUpdate ,
                                        totalAmount: totalAmountUpdate
                                    };
                                    ImportGood.findByIdAndUpdate(reqImportDetail.idImport, updateImportObj, function(err, importData) {
                                    if (err){
                                        console.log(err);
                                        return callback(err);
                                    }else{
                                        //UPdate amount table Import_googd_Detail
                                        dataDetail.amount = req.body.amount;
                                        dataDetail.save(function(err){
                                        if(err){
                                            console.log(err);
                                            return callback(err);
                                        }
                                        else{
                                             callback(null);   
                                        }
                                        }); 
                                    }
                                  });
                                }
                            
                            }});
                          
                        }
                        
                     }else{
                         //Không tìm thấy mã sản phẩm cần cập nhật
                         callback('error');
                     }
                })
            },
             ], function(err){
                if(err){
                    console.log(err)
                    sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
                }else{
                    if(lstErr.length){
                         console.log(lstErr);
                         sendJsonResponse(res,404, {lstErr: lstErr});
                    }else{
                        console.log('Done');
                        sendJsonResponse(res,200,"Success");   
                    }
                  
                }
             });
        }
    }
   var deleted = function(req,res){
      
        var docs= [];
        var lstErr = [];
        async.waterfall([
            //get list importGoodDEtail 
            function(callback){
                ImportGoodDetail.find({idImport :  new mongoose.mongo.ObjectID(req.params.import_goodId)}, function (err, goodDetails) {  
                if(err){
                    console.log(err);
                    return callback(err); 
                }else{
                    callback(null, goodDetails);
                }
                }) 
            }
            ,
            //Update all qty have id product in goodDetails
            function(goodDetails, callback){
                 var tasks = goodDetails.map(function(item){
                    return function(callback)
                    {
                       Product.findById(item.product, function(err,product){
                        if(err){
                            console.log(err);
                            return callback(err); 
                        }
                        else{
                            var qtyTotal =  product.qty -  item.amount ;
                            //So luong ton qty hien tai khong du de tru so luong nhap ra. Do san pham do da
                            //thuc hien ban san pham nay voi 1 so luong, dan toi khong du so luong huy hoa don'
                            if(qtyTotal < 0){
                                lstErr.push('Số lượng tồn của sản phẩm '+ product.productId + " là "+ product.qty + " không đủ cung cấp để xóa đơn nhập hàng");
                                 callback(null);
                            }else{
                                
                                var updateObj = {
                                qty: qtyTotal
                                };
                                Product.findByIdAndUpdate(item.product, updateObj, function(err, model) {
                                if (err){
                                    return callback(err); 
                                }else{
                                    //docs  luu lai nhung value product da update truoc do.
                                    //Khong the callback(null, docs)  o day vi no dang o vong lap, dan toi tra ve bi trung du lieu
                                    //Ket qua tra ve xe nam o parallel
                                    docs.push(product);
                                    callback(null);
                                }
                                });
                            }   
                        }
                    });
                    
                    }
                });
                async.parallel(tasks, function(err){
                    if (err){ 
                        callback(err);
                    }
                    else {
                        //docs: Noi chua nhung gia tri da update thanh cong truoc do
                        callback(null,docs);
                    }
                });
            }
        ], function(err, results){
            if(err){
                 console.log(err)
                 sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }else{
                if(lstErr.length){
                     console.log(lstErr);
                      async.each(results, rollback, function () {   
                           console.log('Rollback import delete  done.');
                     });
                     sendJsonResponse(res,404, {lstErr: lstErr});
                }else{
                   
                       async.waterfall([
                        function(callback){
                            req.import_good.isdelete = true;
                            req.import_good.save(function(err){
                                if(err){
                                    console.log(err);
                                    return callback(err);
                                }
                                else{
                                    callback(null);
                                }
                            })
                        }],
                        function(err){
                            if(err){
                                console.log(err);
                                sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
                            }else{
                                console.log('Done');
                                sendJsonResponse(res,200,"Success");
                            }
                               
                        })
        
                }
               
            }
        });
        function rollback (doc, callback) {
        if (doc == null  || doc._id == "") {
            callback();
        }
        else {
                //Cap nhat lai so luong qty ban dau 
               var dataUpdate = {
                    qty  : doc.qty,
                }
                Product.findByIdAndUpdate({_id :  new mongoose.mongo.ObjectID(doc._id)} , dataUpdate, function (err, doc) {  
                if(err)
                {
                    console.log('Error Rolled-back document: ' + err);
                }
                console.log('Rolled-back document: ' + doc._id);
                callback();
                });
            
                }
            }
     };
    return {
        post: post,
        get:get,
        getItem: getItem,
        patch: patch,
        delete : deleted
    }
}
module.exports = import_goodController;