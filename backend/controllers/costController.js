var async    = require('async'),
    mongoose = require('mongoose');

var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};

var costController = function(Cost){

    var post = function(req, res){
        var lstErr = [];
        var lstFieldErr = [];
        var data = req.body;

        var cost = new Cost({
            name: data.name,
            price: data.price,
        });
       
        //check error
        var error =cost.validateSync();
       
        if (error) {
            if (error.name == 'ValidationError') {
               
                for (field in error.errors) {
                   lstFieldErr.push(field);
                   lstErr.push(error.errors[field].message);
                }
            }
        }
        
        if (lstErr.length){
             console.log(lstErr);
             return sendJsonResponse(res,404,{	lstErr:lstErr ,lstFieldErr: lstFieldErr});	
        }else{
            cost.save(function(err){
            if(err){
                sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }
            else{
                sendJsonResponse(res,200,cost);
            }
        });
        }
       
    }
   
    var get = function(req,res){

        res.setHeader('Access-Control-Allow-Origin', '*');
        var query = {};
        //Query tu use. giong find
        // if(req.query.name)
        // {
        //     query.name = req.query.name;
        // }
        //Check TH chi hien thi nhung cost chua delete
        query.isdelete = false;
        Cost.find(query, function(err,costs){

            if(err){
                   sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }            
            else {
                sendJsonResponse(res,200,costs);
            }
        });
    }   
    var getItem = function(req,res){   
        sendJsonResponse(res,200,req.cost);  
    }
    var patch = function(req,res){
       //res.setHeader('Access-Control-Allow-Origin', '*');
        var lstErr = [];
        var lstFieldErr = [];
        // if(req.body._id){
        //     delete req.body._id;
        // }
      
        // for(var p in req.body)
        // {
        //     req.cost[p] = req.body[p];
        // }
        req.cost['name'] = req.body['name'];
        req.cost['price'] = req.body['price'];
        var error = req.cost.validateSync();

        if (error) {
            if (error.name == 'ValidationError') {
               
                for (field in error.errors) {
                   lstFieldErr.push(field);
                   lstErr.push(error.errors[field].message);
                }
            }
        }
        //res.send(lstErr);
        if (lstErr.length){
             console.log(lstErr);
             return sendJsonResponse(res,404,{	lstErr:lstErr ,lstFieldErr: lstFieldErr});	
        }else{
            req.cost.save(function(err){
            if(err){
                 sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }
            else{
               sendJsonResponse(res,200,req.cost);
            }
            });
        }
    }
   var deleted = function(req,res){
       //Update co isdelete = true;
        req.cost.isdelete = true;
        req.cost.save(function(err){
            if(err)
                res.status(500).send(err);
            else{
                sendJsonResponse(res,200,req.cost);
            }
        });
     };
    return {
        post: post,
        get:get,
        getItem: getItem,
        patch: patch,
        delete : deleted
    }
}
module.exports = costController;