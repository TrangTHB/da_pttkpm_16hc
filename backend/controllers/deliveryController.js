var async    = require('async'),
    mongoose = require('mongoose');

var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};

var deliveryController = function(DeliveryModel){

    //var post = function(req, res){
       
        //var deliveries = new DeliveryModel(req.body);
        //check error
        //var error = deliveries.validateSync();
        //if (error && error.errors['customer_name']) {
            //sendJsonResponse(res,404,{	lstErr: [ error.errors['customer_name'].message]});	
            //return;
        //}
        //publishers.save(function(err){
            //if(err){
                //sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            //}
            //else{
                //sendJsonResponse(res,200,publishers);
            //}
        //});
    //}
   
    var get = function(req,res){
        res.setHeader('Access-Control-Allow-Origin', '*');
        var query = {};
        //Query tu use. giong find
        //if(req.query.name)
        //{
            //query.name = req.query.name;
        //}
        //Check TH chi hien thi nhung publisher chua delete
        query.isdelete = false;
        DeliveryModel.find(query, function(err,deliveries){

            if(err){
                   sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }            
            else {
                sendJsonResponse(res,200,deliveries);
            }
        });
    }   
    var getItem = function(req,res){   
        res.json(req.delivery.toJSON());
    }
    var patch = function(req,res){
       //res.setHeader('Access-Control-Allow-Origin', '*');
        //if(req.body._id){
            //delete req.body._id;
        //}
      
        //for(var p in req.body)
        //{
            //req.delivery[p] = req.body[p];
        //}
		let deli = req.delivery;
		deli.customer_name = req.body.customer_name;
		deli.phone = req.body.phone;
		deli.address = req.body.address;
		deli.note = req.body.note;
        var error = deli.validateSync();
        if (error && error.errors['customer_name']) {
            res.send(error.errors['customer_name'].message);
            //Fix loi:Can't set headers after they are sent . Resolve: Dung lenh return
            return;
        }
		if (error && error.errors['phone']) {
            res.send(error.errors['phone'].message);
            //Fix loi:Can't set headers after they are sent . Resolve: Dung lenh return
            return;
        }
		if (error && error.errors['address']) {
            res.send(error.errors['address'].message);
            //Fix loi:Can't set headers after they are sent . Resolve: Dung lenh return
            return;
        }
		if (error && error.errors['cost']) {
            res.send(error.errors['cost'].message);
            //Fix loi:Can't set headers after they are sent . Resolve: Dung lenh return
            return;
        }
		
        deli.save(function(err){
            if(err){
                 sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }
            else{
               sendJsonResponse(res,200,deli);
            }
        });
    }
    return {
        //post: post,
        get:get,
        getItem: getItem,
        patch: patch
    }
}
module.exports = deliveryController;