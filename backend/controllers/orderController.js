var async    = require('async'),
    mongoose = require('mongoose');


var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};

var orderController = function(Order, OrderDetail, Product, DeliveryModel){
    var MyModel =OrderDetail ;

    var get = function(req,res){
        res.setHeader('Access-Control-Allow-Origin', '*');
         
        Order.find({isdelete :  false})
        //Loai bo cot orderDetail ra khoi select
        .select('-orderDetail')
        .populate([{
            path: 'user',
            select: ['name', "address"]
        },
        {
            path: 'statement',
            select: ['name']
        }
        ])
        .exec(function(err, orders)
        {
            if (err){
                  sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }
            else  {
                  
                  sendJsonResponse(res,200,orders);
            }
        });
    }  
    var getItem = function(req,res){  
        OrderDetail.find({order :  new mongoose.mongo.ObjectID(req.params.orderId)})
        .populate([
         {
            path: "product",
            select: ['name', 'img_main']
         }
         ])
        .exec(function(err, order)
         {
            if(err){
                    sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
                }
                else if(order)
                {
                    req.order = order;
                    sendJsonResponse(res,200,req.order); 
                }
                else
                {
                     sendJsonResponse(res,404,{
						lstErr: ['Không tìm thấy chi tiết đơn nhập hàng dựa vào mã chi tiết đơn nhập']
					});
                }
        });
            
    } 
    
    var patch = function(req,res){
        if(req.body._id){
            delete req.body._id;
        }
        var lstErr = [];
        if(!req.body.statement){
           req.order['statement'] =  req.body['statement'];
           var error = req.order.validateSync(); 
           if (error && error.errors['statement']) {
                for (field in error.errors) {
                    lstErr.push(error.errors[field].message); 
                }
                sendJsonResponse(res,404,{	lstErr:  lstErr});	
            //Fix loi:Can't set headers after they are sent . Resolve: Dung lenh teturn
            return;
            }
        }
        
        req.order.statement = ObjectId(req.body.statement);
    
        req.order.save(function(err){
            if(err){
                 sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }
            else{
                 console.log("Done");
                 sendJsonResponse(res,200,req.order);
            }
        });
    }
    var deleted = function(req,res){
        //Update co isdelete = true;
        async.waterfall([
                //Update column isdelete = true table order
                function(callback){
                     req.order.isdelete = true;

                     req.order.save(function(err){
                        if(err){
                            console.log(err);
                            return callback(err);
                        }
                        else{
                            console.log('Delete order success!');
                            callback(null);
                        }
                     })
                },
                //Update isdelete if status is delivery into table delivery 
                 function(callback){
                        if(req.order.typePayment == 2){
                        DeliveryModel.find({order: req.order._id}, function(err,delivery){
                        if(err){
                            console.log(err);
                            return callback(err)
                        }else{
                              var updateObj = {
                                isdelete  : true,
                                }
                                DeliveryModel.findByIdAndUpdate(delivery[0].id, updateObj, function(err, model) {
                                if (err){
                                    console.log(err);
                                    return callback(err)
                                }else{
                                      callback(null)
                                }
                                })

                        }
                      
                        });
                       
                    }else{
                         callback(null)
                    }
                },
                //Update all product with qty into table product
                function(callback){
                    OrderDetail.find({order :  new mongoose.mongo.ObjectID(req.params.orderId)}, function (err, orderDetail) {  
            
                    if(err){
                            console.log(err);
                           return callback(err);
                    }else{
                        var tasks = orderDetail.map(function(item){
                        return function(callback)
                        {      
                            Product.findById(item.product, function(err,product){
                                if(err){
                                    console.log(err);
                                    return callback(err);
                                }
                                else{
                                    
                                    var qtyTotal =  product.qty +  item.amount ;
                                    var updateObj = {
                                        qty: qtyTotal
                                    };
                                    
                                    Product.findByIdAndUpdate(item.product, updateObj, function(err, model) {
                                    if (err){
                                        console.log(err);
                                        return callback(err);
                                    }else{
                                        callback(null);
                                    }
                                  
                                });
                                
                                }
                            });
                        
                                }
                            });
                        async.parallel(tasks, function(err){
                        if (err){ 
                            return callback(err);
                        }
                        else {
                                callback(null);
                            }
                        });

                 } });
              }
        ], function(err, results){
            if(err){
                console.log(err)
                sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }else{
                console.log('Done');
                sendJsonResponse(res,200,"Success");
            }
        });

    };

     var post = function(req, res){

       if (!req.auth 
            || (req.auth.role != 'seller'
             && req.auth.role != 'manager' 
             && req.auth.role != 'admin')){ 
                req.status(403).send('Authorization is required');
        }
        else
        {
            let currentUser = req.auth.userID;
            var data = req.body;
            var statement;
            if(data.typePayment == 1){
                //Da thanh toan
                statement = ObjectId("58efa7c0a9e88921ec7a2d6d");
            }else{
                //Chua giao hang
                statement = ObjectId("5922bedfbab96280ae7eb33e");
            }
        
            var reqOrder = new Order({
                user:  currentUser,
                totalMoney: data.totalMoney,
                totalAmount: data.totalAmount,
                statement: statement,
                typePayment: data.typePayment
            });
        
            var errorOrder = reqOrder.validateSync();

            var lstErr = [];
            var lstFieldErr = [];
            //Check error TH Order
            if (errorOrder) {
                if (errorOrder.name == 'ValidationError') {
                
                    for (field in errorOrder.errors) {
                    lstFieldErr.push(field);
                    lstErr.push(errorOrder.errors[field].message);
                    }
                }
            }
            //Check error input for delivery if choose delivery 
            //TH payment equay delivery
            var delivery;
            if(statement =="5922bedfbab96280ae7eb33e"){
                delivery = new DeliveryModel({
                    customer_name: data.delivery.customer_name,
                    order: ObjectId(reqOrder._id),
                    phone: data.delivery.phone,
                    address: data.delivery.address,
                    cost: data.delivery.cost,
                    note: data.delivery.note
                });
                var errorDelivery = delivery.validateSync();
                if (errorDelivery) {
                    if (errorDelivery.name == 'ValidationError') {
                    
                        for (field in errorDelivery.errors) {
                        lstFieldErr.push(field);
                        lstErr.push(errorDelivery.errors[field].message);
                        }
                    }
                }
            }

            var tasks = data.orderDetail.map(function(od){
            return function(callback)
            {
                var reqOderDetail = new OrderDetail({
                    order: ObjectId(reqOrder._id),
                    amount: od.amount,
                    price: od.price,
                    product: od.product._id,
                });
            
                var errorOderDetail = reqOderDetail.validateSync();
                    if (errorOderDetail) {
                    if (errorOderDetail.name == 'ValidationError') {
                    
                        for (field in errorOderDetail.errors) {
                            lstFieldErr.push(field+ "_"+ od.product.productId);
                            lstErr.push(errorOderDetail.errors[field].message + " tại sản phẩm "+ od.product.productId);
                        }
                    }
                }
            async.waterfall([
                    function(callback){
                        Product.findById(od.product, function(err,product){
                        if(err){
                            console.log(err);
                            callback(error);
                        }
                        else{
                            var amount = od.amount;
                            //Muc dich get qty from db chu ko phai from frontend gui qua. Check la phai check from db
                            var qty = product.qty;
                            if(amount > qty) {
                                lstFieldErr.push('amount_'+od.product.productId);
                                lstErr.push('Số lượng mua sản phẩm ' + od.product.productId + " có số lượng vượt quá số lượng tồn kho: "+ qty);
                                
                            }
                            callback(null,reqOderDetail)
                        
                            }
                    });
                }], function(err, data){
                    if(err){
                        return callback(err);
                    }else{
                        callback(null, data);
                    }
                });
            // return reqOderDetail;
                }
            });

            async.parallel(tasks, function(err, orderdetails){
                if (err){ //Nếu bất kì 1 task nào trong list bị lỗi rơi vào đây
                    console.log(err);
                    sendJsonResponse(res,404,err); 
                }
                else {
                if (lstErr.length){
                        console.log(lstErr);
                        return sendJsonResponse(res,404,{	lstErr:lstErr ,lstFieldErr: lstFieldErr});
                }else{
                    reqOrder.orderDetail =  orderdetails.map(function(od){
                            return od._id;
                    })
                
                    var docs= {};
                    docs['order'] = {};
                    docs['orderDetail'] = [];

                    var check = 0;
                    async.waterfall([
                        //TH ORDER DETAIL
                        function(callback){
                            var check = 0;
                                async.each(orderdetails,function(od, callback){
                                    
                                    od.save(function(err){
                                    if(err){
                                        //Khong return callback o day vi nieu return o day thi no xe tra ve docs = null.
                                        //Check de luu lai nhung docs da duoc insert success trong qua trinh
                                        //muc dich phu vu cho viec roolback data
                                        check =1;
                                    }else{
                                        docs['orderDetail'].push(od);
                                    }
                                        callback(null, docs);
                                    
                                });
                            },
                            function(err) {
                                if( err ) {
                                    //Do doan vong lap define da mac dinh no la khong co errror .
                                    //TH minh check no la error khi bien check = 1
                                    //Khong xu ly cho nay chi can ktra check
                                } else {
                                    if(check == 1){
                                            console.log('A order detail failed to process');
                                            return callback('loi',docs);

                                    } else{
                                        console.log('All order detail have been processed successfully');
                                        callback(null, docs);
                                    }
                                }
                            })},
                        //TH ORDER
                        function(docs , callback){
                                reqOrder.save(function(err){ 
                                    if (err){
                                        console.log('A order failed to process');
                                        return callback(err, docs);
                                    }
                                    console.log('Order have been processed successfully');
                                    docs['order'] = reqOrder;
                                    callback(null, docs);
                                })
                        }, 
                        //TH DAT HANG 
                        function(docs , callback){
                        //Nieu la trang thai giao hang thi tiep thuc insert vao bang giao hang, 
                        //else call back success--> final
                            if(statement == "5922bedfbab96280ae7eb33e"){
                            
                                delivery.save(function(err){ 
                                    if (err){
                                    
                                        console.log('Delivery insert fail');
                                        return callback(err, docs);
                                    }
                                    console.log('Delivery have been processed successfully');
                                    //callback(null, docs);
                                })
                            };
                            callback(null, docs);
                        
                        }, 
                    
                    ],
                    function (errs, results) {
                        if (errs) {
                        //Do danh sach result tra ve co dang json chua kq insert cua order va orderdetail
                        //Nen gio phari check is nieu ton tai ket qua insert cua order hoac orderdetail thi roolback no lai
                            if(results.order.id != undefined){
                                //Mymodel: dung de check nieu truoc do co insert cho table Order
                                MyModel = Order;
                                rollback(results.order, function(){
                                    console.log('Rollback order done.');
                                });
                            }
                            if(results.orderDetail.length){
                                MyModel = OrderDetail;
                                async.each(results.orderDetail, rollback, function () {
                                    console.log('Rollback order detail done.');
                                });
                            }
                            
                            sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
                        
                        } else {

                                var tasks = results.orderDetail.map(function(item){
                                return function(callback)
                                {
                                    Product.findById(item.product, function(err,product){
                                        if(err){
                                            console.log(err);
                                            callback(error);
                                        }
                                        else
                                        {
                                            var qtyTotal =  product.qty -  item.amount ;
                                            var updateObj = {
                                                qty: qtyTotal
                                            };            
                                            Product.findByIdAndUpdate(item.product, updateObj, function(err, model) {
                                                if (err){
                                                    callback(error); 
                                                }
                                                else callback(null, model);
                                            });
                                        }
                                    });
                                }
                            });
                            async.parallel(tasks, function(err, result){
                                if (err){ //Nếu bất kì 1 task nào trong list bị lỗi rơi vào đây
                                    console.log(err);
                                    sendJsonResponse(res,404,err); 
                                
                                }
                                else {
                                    console.log("Done");
                                    sendJsonResponse(res,200,result); 
                                //Result lúc này là mảng các Product đã được update
                                }
                            });
                        }
                    });

                    function rollback (doc, callback) {
                    if (doc == null) {
                        callback();
                    }
                    else {
                            MyModel.findByIdAndRemove({_id :  new mongoose.mongo.ObjectID(doc._id)}, function (err, doc) {  
                            if(err)
                            {
                                console.log('Error Rolled-back document: ' + err);
                            }
                            console.log('Rolled-back document: ' + doc._id);
                            callback();
                            });
                        
                            }
                        }
                

                }	

                }
            });
        }
        
    } 
   
    return {
        get:get,
        getItem: getItem,
        patch: patch,
        delete : deleted,
        post: post,
    }
}
module.exports = orderController;