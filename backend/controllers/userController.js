var cloudinary = require('cloudinary'),
	mongoose = require('mongoose'),
	async = require('async');
cloudinary.config({
	cloud_name: 'dxnapa5zf',
	api_key: '219348637198157',
	api_secret: 'FJl9rCE5dTS_kgkX_rPvUyMTwZY'
});


var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};
var userController = function(User, Role){

    var get = function(req,res){
        let lstAnd = [];
        lstAnd.push({isdelete: false});
        User.find({$and: lstAnd})
            .populate([{
                     path: 'role',
                     select: ['name', 'name_desc'],
                }])
            .select({"_id": 1, "name": 1, "username": 1, 'role': 1, 'isdelete': 1, 'updatedAt': 1, 'createdAt': 1, 'address': 1, 'gender': 1, 'img': 1})
            .exec(function(err, users){
                if (err){
                    res.status(404).json({lstErr: ['Có lỗi trong quá trình xử lý']});
                }
                else res.json(users);
            });

    }
    var getInfo = function(req, res){
        if (!req.auth)
        {
            res.status(401).send('Authorized is required for this API');            
        }
        else
        {
            User.findOne({username: req.auth.username})
                .populate([{
                     path: 'role',
                     select: ['name', 'name_desc'],
                }])
                .select({"_id": 0, "name": 1, "username": 1, 'role': 1, 'isdelete': 1, 'updatedAt': 1, 'createdAt': 1, 'address': 1, 'gender': 1, 'img': 1})
                .exec(function(err, users){
                    if (err)
                    {
                        res.status(404).json({lstErr: ['Có lỗi trong quá trình xử lý']});

                    }
                    else
                    {
                        res.json(users);
                    }
                })
        }
    }
    var getRoles = function(req, res){
        Role.find({})
            .select({'_id': 1, 'name': 1, 'name_desc': 1})
            .exec(function(err, roles){
                if (err){
                    res.status(404).json({lstErr: ['Có lỗi trong quá trình xử lý']});
                }
                else res.json(roles);
            });
    }

    var getDetail = function(req, res){
        if (!req.user){
            res.status(404).json({lstErr: ['Có lỗi trong quá trình xử lý']});
        }
        else res.json(req.user);
    }

    var put = function(req, res){
        let model = JSON.parse(req.body.model);
        let lstErr = [];
        if (!model.name || model.name == '' || model.name.length < 6)
        {
            lstErr.push('Tên không được để trống và chiều dài >= 6');
        }
        if (!model.gender)
        {
            lstErr.push('Chưa có thông tin giới tính');
        }
        if (!model.role || model.role == '' || model.role == "-1"){
            lstErr.push('Chưa chọn mã loại nhân viên');
        }
        if (lstErr.length > 0){
            res.status(404).json({lstErr: lstErr});
        }
        else
        {
            async.waterfall([
                function(callback){
                    let User = req.user;
                    User.name = model.name;
                    User.address = model.address;
                    User.role = model.role;
                    User.gender = model.gender;
                    callback(null, User);
                
                },
                function(User, callback){
                    if (req.file){
                        cloudinary.uploader.upload(req.file.path, function(result){
                            User.img = result.url;
                            callback(null, User);
                        });
                    }
                    else callback(null, User);
                }
            ], function(err, User){
                if (err){
                     lstErr.push('Có lỗi trong quá trình xử lý');
                     res.status(404).json({lstErr: lstErr});
                }
                else
                {
                    User.save(function(err){
                        if (err){
                            console.log(err);
                             lstErr.push('Có lỗi trong quá trình xử lý');
                             res.status(404).json({lstErr: lstErr});
                        }
                        else{
                            res.json(User);
                        }
                    })
                }
            })
        }
    }

   var deleteUser = function(req, res){
        if (!req.auth){
            res.status(403).send('Authorized is required');
        }
        else
        {
            if (!req.user){
            res.status(404).json({lstErr: ['Có lỗi trong quá trình xử lý']});
            }
            else
            {
                if (req.user._id.toString() == req.auth.userID.toString())
                {
                    res.status(404).json({lstErr: ['Không thể xóa user đang đăng nhập hiện tại']});    
                }
                else
                {
                    let User = req.user;
                    User.isdelete = true;
                    User.save(function(err){
                        if (err){
                            res.status(404).json({lstErr: ['Có lỗi trong quá trình xử lý']});
                        }
                        else
                        {
                            res.json({result: 0});
                        }
                    });
                }
                
            }
        }

    }
   
    return {
        get:get,
        getInfo: getInfo,
        getRoles: getRoles,
        getDetail: getDetail,
        put: put,
        deleteUser: deleteUser
    }
}
module.exports = userController;