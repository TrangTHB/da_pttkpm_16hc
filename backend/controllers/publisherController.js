var async    = require('async'),
    mongoose = require('mongoose');

var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};

var publisherController = function(Publisher, Product){

    var post = function(req, res){


        
       var data = req.body;
        var publishers = new Publisher(
        {
            name: data.name
        }
        );
        //check error
        var error = publishers.validateSync();
        if (error && error.errors['name']) {
            sendJsonResponse(res,404,{	lstErr: [ error.errors['name'].message]});	
            return;
        }else{
            publishers.save(function(err){
            if(err){
                sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }
            else{
                sendJsonResponse(res,200,publishers);
            }
            });

        }
       
    }
   
    var get = function(req,res){
        res.setHeader('Access-Control-Allow-Origin', '*');
        var query = {};
        //Query tu use. giong find
        // if(req.query.name)
        // {
        //     query.name = req.query.name;
        // }
        //Check TH chi hien thi nhung publisher chua delete
        query.isdelete = false;
        Publisher.find(query, function(err,publishers){
            if(err){
                   sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }            
            else {
                sendJsonResponse(res,200,publishers);
            }
        });
    }   
    var getItem = function(req,res){   
        sendJsonResponse(res,200,req.publisher);  
    }
    var patch = function(req,res){
       //res.setHeader('Access-Control-Allow-Origin', '*');
        // if(req.body._id){
        //     delete req.body._id;
        // }
        
        // for(var p in req.body)
        // {
        //     req.publisher[p] = req.body[p];
        // }
        req.publisher['name'] = req.body['name'];
        var error = req.publisher.validateSync();
        if (error && error.errors['name']) {
            res.send(error.errors['name'].message);
            //Fix loi:Can't set headers after they are sent . Resolve: Dung lenh return
            return;
        }else{
            req.publisher.save(function(err){
            if(err){
                 sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }
            else{
               sendJsonResponse(res,200,req.publisher);
            }
            });
        }
        
    }
   var deleted = function(req,res){
       //Update co isdelete = true with query is no have product have this publisher 
       var lstErr = [];
        var lstFieldErr = [];
        async.waterfall([
            function(callback){
                 Product.find({publisher: req.params.publisherId}, function(err,products){
                    if(err){
                        console.log(err);
                        return callback(err);
                    }else{
                        if(products.length){
                             lstErr.push('Nhà cung cấp này đã tồn tại trong bảng sản phẩm. Không thể xóa!');
                             callback(null);
                        }else{
                            req.publisher.isdelete = true;
                            req.publisher.save(function(err){
                                if(err){
                                    return callback(err);    
                                }
                                else{
                                    callback(null);
                                }
                            });
                          
                        }
                    }
                      
                 });
            }
        ], function(err){
            if(err){
                sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }else{
                if(lstErr.length){
                   sendJsonResponse(res,404, {lstErr: lstErr});
                }else{
                      sendJsonResponse(res,200,"Success");
                }
            }
        });

     };
    return {
        post: post,
        get:get,
        getItem: getItem,
        patch: patch,
        delete : deleted
    }
}
module.exports = publisherController;