var async    = require('async'),
    mongoose = require('mongoose');

var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};

var reportController = function(Order,ImportGood){

    
    var get = function(req,res){ //query ?month_begin=2&month_end=3&years=2011,2012 -> Neu khong co gi mac 
        //dinh la lay 1->12 cua nam hien tai
       
       let month_begin = 1;
       let month_end = 12;
       if (req.query.month_begin){
           month_begin = Number(req.query.month_begin);
       }
       if (req.query.month_end){
           month_end = Number(req.query.month_end);
       }
       
       var year_tk = {};
        //Chi thong ke cho 3 nam gan nhat
       var date = new Date();


       let lstYear = [date.getFullYear()];
       if (req.query.years){
           lstYear = req.query.years.split(',').map(y => Number(y));
       }

       lstYear = lstYear.map((year) => {
           let year_object = {};
           year_object.year = year;
           year_object.totalAmountOrder = 0;
           year_object.totalMoneyOrder = 0;
           year_object.totalAmountImportGood = 0;
           year_object.totalMoneyImportGood = 0;
           year_object.profit = 0;
           let months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
           year_object.months = months.map((month) => {
               let month_object = {};
               month_object.month = month;
               month_object.totalAmountOrder = 0;
               month_object.totalMoneyOrder = 0;
               month_object.totalAmountImportGood = 0;
               month_object.totalMoneyImportGood = 0;
               month_object.profit = 0;
               return month_object;
           });
           return year_object;
       })
        async.waterfall([
            //get thong tin thong ke orrder
            function(callback){
                Order.aggregate([
               { $match : { isdelete : false } } ,
              
                {
                    $group : { 
                        _id : { year: { $year : "$createdAt" }, month: { $month : "$createdAt" }}, 
                        totalAmountOrder: {$sum:  "$totalAmount"},
                        totalMoneyOrder: {$sum: "$totalMoney"},
                    }
                
                },
                ], function (err, result) {
                    if (err) {
                        next(err);
                        callback(err);
                    } else {
                    
                        result.forEach(function(item){
                            let y = lstYear.find(h => h.year == item._id.year);
                            if (y){
                                y.totalAmountOrder += item.totalAmountOrder;
                                y.totalMoneyOrder += item.totalMoneyOrder;
                                y.months[item._id.month -1].totalAmountOrder = item.totalAmountOrder;
                                y.months[item._id.month -1].totalMoneyOrder = item.totalMoneyOrder;
                            }
                        });  
                        callback(null, lstYear);
                    }
                });
            },
             //get thong tin thong ke nhap hang
            function(lstYear, callback){
                ImportGood.aggregate([
               { $match : { isdelete  : false } },
                {
                    $group : { 
                        _id : { year: { $year : "$createdAt" }, month: { $month : "$createdAt" }}, 
                        totalAmountImportGood: {$sum:  "$totalAmount"},
                        totalMoneyImportGood: {$sum: "$totalMoney"},
                    }
                
                },
                ], function (err, result) {
                    if (err) {
                        next(err);
                        callback(null);
                    } else {
                        result.forEach(function(item){
                            let y = lstYear.find(h => h.year == item._id.year);
                            if (y){
                                y.months[item._id.month - 1].totalAmountImportGood = item.totalAmountImportGood;
                                y.months[item._id.month - 1].totalMoneyImportGood = item.totalMoneyImportGood;
                                y.totalAmountImportGood += item.totalAmountImportGood;
                                y.totalMoneyImportGood += item.totalMoneyImportGood;
                            }
                    }); 
                        callback(null, lstYear); 
                        
                    }
                    
                });
                        
            }
        ], 
        function (errs, lstYear) {
            if (errs) {
                console.log(errs);
                sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
            }else{
                lstYear.forEach(function(y, index){
                    y.months.forEach(function(m, index){
                        m.profit = m.totalMoneyOrder - m.totalMoneyImportGood;
                        y.profit += m.profit;
                    })
                    y.months = y.months.filter((m) => {
                        if (m.month >= month_begin && m.month <= month_end){
                            return true;
                        }
                        return false;
                    })
                });
                
                console.log('Done');
                sendJsonResponse(res,200,lstYear);
            }
        });
    }   
  
    return {
     
        get:get,
      
    }
}
module.exports = reportController;