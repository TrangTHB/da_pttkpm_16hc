var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cors = require('cors');
var jwt = require('jsonwebtoken');
var async = require('async');



//connection to db
var db = mongoose.connect('mongodb://admin:123456@ds155490.mlab.com:55490/quanlyshopquanao');

//var Book = require('./models/bookModel');
//var Product = require('./models/productModel');
var Role = require('./models/roleModel');
var User = require('./models/userModel');
var Category = require('./models/categoryModel')
var Type = require('./models/typeModel');
var Story = require('./models/storyModel');
var Publisher = require('./models/publisherModel');
var Statement = require('./models/statementModel');
var Order = require('./models/orderModel');
var OrderDetail = require('./models/orderDetailModel');
var Product = require('./models/productModel');
var Cost = require('./models/costModel');
var ImportGood = require('./models/import_goodModel');
var ImportGoodDetail = require('./models/import_good_detailModel');
var DeliveryModel = require('./models/deliveryModel');


var app = express();
app.use(cors());

var port = process.env.PORT || 5000;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// Add headers


//var bookRouter = require('./Routes/bookRoutes')(Book, User);
//var tokenRouter = require('./Routes/tokenRoutes');
var storyRouter = require('./Routes/storyRoutes')(Story, Category);
// <<<<<<< HEAD
// var productRouter = require('./Routes/productRoutes')(Product);
var categoryRouter = require('./Routes/categoryRoutes')(Category, User, Type,Product);

var productRouter = require('./Routes/productRoutes')(Product, ImportGoodDetail, OrderDetail);
// var categoryRouter = require('./Routes/categoryRoutes')(Category, User, Type);
// >>>>>>> d1b48c48096fbb4df9b290b07a71ee025d5f90dd
var typeRouter = require('./Routes/typeRoutes')(Type);
var authRouter = require('./Routes/authRoutes')(User);
var userRouter = require('./Routes/userRoutes')(User, Role);
var publisherRouter = require('./Routes/publisherRoutes')(Publisher, Product);
var orderRouter = require('./Routes/orderRoutes')(Order, OrderDetail, Product, DeliveryModel);
var statementRouter =  require('./Routes/statementRoutes')(Statement);
var tokenRouter = require('./Routes/tokenRoutes')(User, Role)
var costRouter =  require('./Routes/costRoutes')(Cost);
var import_goodRouter =  require('./Routes/import_goodRoutes')(ImportGood, ImportGoodDetail, Product);
var reportRouter = require('./Routes/reportRoutes')(Order,ImportGood);
var deliveryRouter = require('./Routes/deliveryRoutes')(DeliveryModel);
//var import_good_detailRouter =  require('./Routes/import_good_detailRoutes')(ImportGoodDetail);

//app.use('/api/Books', bookRouter);
//app.use('/api/Products', productRouter)

//Middleware collects info from the token

app.use('/api', function(req, res, next){
    var auth = req.headers["authorization"];
    if (auth)
    {
        var token = auth.split(' ')[1];

        jwt.verify(token, 'khintmam', {algorithm: 'HS256'}, function(err, verified){
            if (err){
                req.auth = undefined;
                next();
            }
            else
            {
                req.auth = verified;
                User.findOne({username: req.auth.username, isdelete: false})
                    .populate([{
                        path: 'role',
                        select: ['name']
                    }])
                    .exec(function(err, user){
                        if (err){
                            res.status(404).json({lstErr: 'Có lỗi trong quá trình xử lý'}).end();
                        }
                        else
                        {
                            if (!user || user.role.name != req.auth.role){
                                res.status(401).send('Please login again to confirm your account set role exactly');
                            }
                            else {
                                 req.auth.userID = user._id;
                                 next();
                            }
                        }
                    });
            }
            
        });
    }
    else {
        req.auth = undefined;
        next();
    }
    
});
app.use('/token', tokenRouter);
app.use('/api/stories/', storyRouter);
app.use('/api/categories/', categoryRouter);
app.use('/api/types', typeRouter);
app.use('/api/auth', authRouter);
app.use('/api/user', userRouter);
app.use('/api/publishers', publisherRouter);
app.use('/api/products', productRouter);
app.use('/api/orders', orderRouter);
app.use('/api/status', statementRouter);
app.use('/api/cost', costRouter);
app.use('/api/import_goods', import_goodRouter);
app.use('/api/products', productRouter);
app.use('/api/report', reportRouter);
app.use('/api/delivery', deliveryRouter);
//app.use('/api/import_goods_detail', import_goodRouter);
//app.use('/api/orderDetail', orderDetailRouter);


//Require common:

var common = require('./common.js');
app.use(express.static(__dirname + '/public'));
app.listen(port, function(){
    console.log('The server is listening on PORT: ' + port);
});
