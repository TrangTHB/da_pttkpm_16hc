var express = require("express");
var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};

var routes = function(ImportGood, ImportGoodDetail, Product){
    var import_goodRouter = express.Router();
    var import_goodController = require('../controllers/import_goodController')(ImportGood, ImportGoodDetail, Product)
        import_goodRouter.route('/')
            .post(import_goodController.post)
            .get(import_goodController.get);
       
      import_goodRouter.route('/updateDetail/:IdDetail')
            .patch(import_goodController.patch);
        //Middleware for /:import_goodId
         import_goodRouter.use('/:import_goodId', function(req,res,next){

            ImportGood.findById(req.params.import_goodId, function(err,import_good){
                if(err){
                    sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
                }
                else if(import_good)
                {
                     //Check TH import_good deleted. end middle ware
                    if(import_good.isdelete == true){
                        return res.status(404).json({lstErr: 'Đơn nhập hàng này đã xóa'}).end();
                    }
                    req.import_good = import_good;
                    next();
                }
                else
                {
                     sendJsonResponse(res,404,{
						lstErr: ['Không tìm thấy đơn nhập hàng dựa vào mã đơn nhập']
					});
                }
            });
        });    
        import_goodRouter.route('/:import_goodId')
            .get(import_goodController.getItem)
            .delete(import_goodController.delete)

  return import_goodRouter;
};

module.exports = routes;