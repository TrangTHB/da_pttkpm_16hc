var express = require("express");
var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};

var routes = function(Publisher, Product){
    var publisherRouter = express.Router();
    var publisherController = require('../controllers/publisherController')(Publisher,Product)
        publisherRouter.route('/')
            .post(publisherController.post)
            .get(publisherController.get);
        //Middleware for /:publisherId
         publisherRouter.use('/:publisherId', function(req,res,next){
            Publisher.findById(req.params.publisherId, function(err,publisher){
                if(err){
                    sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
                }
                else if(publisher)
                { 
                     if(publisher.isdelete == true){
                        return res.status(404).json({lstErr: 'Đơn hàng này đã được hủy'}).end();
                    }
                    req.publisher = publisher;
                    next();
                }
                else
                {
                     sendJsonResponse(res,404,{
						lstErr: ['Không tìm thấy nhà cung cấp dựa vào mã nhà cung cấp']
					});
                }
            });
        });    
        publisherRouter.route('/:publisherId')
            .get(publisherController.getItem)
            .patch(publisherController.patch)
            .delete(publisherController.delete)

  return publisherRouter;
};

module.exports = routes;