var express = require("express");
var multer = require('multer');
var crypto = require('crypto');
var mime = require('mime');
var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
    });
  }
});


var upload = multer({ storage: storage });
var routes = function(Product, ImportGoodDetail, OrderDetail){
	var productRouter = express.Router();
	var productController = require('../controllers/productController')(Product, ImportGoodDetail, OrderDetail)
		productRouter.route('/')
			.get(productController.get)
			.post(upload.single('img_main'), productController.post);
    
			//Middleware for the route has productID
		  productRouter.use('/:productID', function(req, res, next){
			Product.findById(req.params.productID, function(err, product){
				if (err){
					res.status(404).send(err);
				}
				else if (product){
					req.product = product;
					next();                    
				}
				else{
					res.status(404).send('Không tìm thấy sản phẩm');
				}
			});
		});
		productRouter.route('/:productID')
            .get(productController.getItem)
            .patch(upload.single('img_main'), productController.patch)
            .delete(productController.delete)
			

    return productRouter;
};
module.exports = routes;