var express = require('express');


var routes = function(User){
   var authRouter = express.Router();
   authRouter.route('/register')
    .post(function(req, res){
        var lstErr = [];
        var lstMessErr = [];
        var model = req.body;
        if (!model.username || model.username.length < 6)
        {
            
            lstMessErr.push('Tên đăng nhập không được trống và độ dài >= 6');
            
        }
        if (!model.name || model.name == '')
        {
           
            lstMessErr.push('Tên không được để trống >= 6');
        }
        if (!model.password || model.password.length < 6)
        {
           
            lstMessErr.push('Mật khẩu không được để trống và độ dài >= 6');
        }
        if (model.repassword != model.password)
        {
           
            lstMessErr.push('Nhập lại mật khẩu không khớp');
        }
      
        if (model.gender == undefined)
        {
            model.gender = true;
        }
     
        if (!model.role || model.role == '' || model.role == "-1"){
            lstMessErr.push('Chưa nhận ra mã loại nhân viên');
        }
        User.findOne({username: model.username}, function(err, user){
            if (err){
              
                lstMessErr.push('Có lỗi xảy ra trong quá trình xử lý');
                res.status(404).json({lstErr: lstMessErr});
            }
            else
            {
                if (user){
                     lstMessErr.push('Đã có thành viên sử dụng username này');
                     res.status(404).json({lstErr: lstMessErr});
                }
                else
                {
                    if (lstMessErr.length > 0)
                    {
                        res.status(404).json({lstErr: lstMessErr});
                    }
                    else
                    {
                        var newuser = new User({
                            username: model.username,
                            password: model.password,
                            name: model.name,
                            address: model.address,
                            gender: model.gender,
                            createdAt: new Date(),
                            updatedAt: new Date(),
                            role: model.role

                        });
                        newuser.save(function(err, _user){
                            if (err){
                                lstMessErr.push('Có lỗi xảy ra trong quá trình xử lý');
                                res.status(404).json({lstErr: lstMessErr});
                            }
                            else
                            {
                                res.status(200).end();
                            }
                        });
                    }
                   
                }
            }
        });
        
    });
    return authRouter;
};
module.exports = routes;