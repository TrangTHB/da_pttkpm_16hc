var express = require("express");
var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};

var routes = function(Order, OrderDetail, Product, DeliveryModel){
    var orderRouter = express.Router();
    // var orderDetailProjection = { 
    //     __v: false,
    //     _id: false,
    //     statement: false,
    //     user: false,
    //     totalMoney: false,
    //     createdAt: false,
    //     updatedAt: false
    // };
    var orderController = require('../controllers/orderController')(Order, OrderDetail, Product, DeliveryModel)
        orderRouter.route('/')
            .get(orderController.get)
            .post(orderController.post);
          //middleware  
           orderRouter.use('/:orderId', function(req,res,next){
                Order.findById(req.params.orderId, function(err,order){
                if(err){
                    console.log(err);
                    return sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
                }
                else if(order)
                {
                    //Check TH order deleted. end middle ware
                    if(order.isdelete == true){
                        return res.status(404).json({lstErr: 'Đơn hàng này đã được hủy'}).end();
                    }
                 
                    req.order = order;
                    next();

                }
                else
                {
                     return sendJsonResponse(res,404,{
						lstErr: ['Không tìm thấy đơn nhập hàng dựa vào mã đơn nhập']
					});
                }
            });
            
        });
        orderRouter.route('/:orderId')
            .get(orderController.getItem)
            //Path: Update trang thai don hang
            .patch(orderController.patch)
            .delete(orderController.delete)
            
  return orderRouter;
};

module.exports = routes;