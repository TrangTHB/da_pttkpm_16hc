var express = require("express");
var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};

var routes = function(Cost){
    var costRouter = express.Router();
    var costController = require('../controllers/costController')(Cost)
        costRouter.route('/')
            .post(costController.post)
            .get(costController.get);
        //Middleware for /:costId
         costRouter.use('/:costId', function(req,res,next){
            Cost.findById(req.params.costId, function(err,cost){
                if(err){
                    sendJsonResponse(res,404, {lstErr: ['Có lỗi trong quá trình xử lý']});
                }
                else if(cost)
                {
                    req.cost = cost;
                    next();
                }
                else
                {
                     sendJsonResponse(res,404,{
						lstErr: ['Không tìm thấy nhà cung cấp dựa vào mã nhà cung cấp']
					});
                }
            });
        });    
        costRouter.route('/:costId')
            .get(costController.getItem)
            .patch(costController.patch)
            .delete(costController.delete)

  return costRouter;
};

module.exports = routes;