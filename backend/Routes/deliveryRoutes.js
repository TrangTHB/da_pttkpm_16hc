var express = require("express");
var multer = require('multer');
var crypto = require('crypto');
var mime = require('mime');
var sendJsonResponse = function(res,status,content){
	res.status(status);
	res.json(content);
};
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
    });
  }
});


var upload = multer({ storage: storage });
var routes = function(DeliveryModel){
	var deliveryRouter = express.Router();
	var deliveryController = require('../controllers/deliveryController')(DeliveryModel)
		deliveryRouter.route('/')
			.get(deliveryController.get);
			//.post(deliveryController.post);
    
			//Middleware for the route has deliveryID
		  deliveryRouter.use('/:deliveryID', function(req, res, next){
			DeliveryModel.findById(req.params.deliveryID, function(err, delivery){
				if (err){
					res.status(404).send(err);
				}
				else if (delivery){
					req.delivery = delivery;
					next();                    
				}
				else{
					res.status(404).send('Không tìm thấy sản phẩm');
				}
			});
		});
		deliveryRouter.route('/:deliveryID')
            .get(deliveryController.getItem)
            .patch(deliveryController.patch)			

    return deliveryRouter;
};
module.exports = routes;