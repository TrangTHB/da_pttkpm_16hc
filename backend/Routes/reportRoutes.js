var express = require("express");


var routes = function(Order,ImportGood){
    var reportRouter = express.Router();
    var reportController = require('../controllers/reportController')(Order,ImportGood)
        reportRouter.route('/')
            .get(reportController.get);
  return reportRouter;
};

module.exports = routes;