var express = require('express');
var jwt = require('jsonwebtoken');
var async = require('async');



var routes = function(User, Role){
   var tokenRouter = express.Router();
  
   tokenRouter.route('/')
    .post(function(req, res){
        let lstErr = [];
        async.waterfall([
            function(callback){
                User.findOne({username: req.body.username,
                   password: req.body.password, isdelete: false})
                   .populate([{
                       path: 'role',
                       select: ['name']
                   }]).exec(function(err, user){
                       if (err){
                           lstErr.push('Có lỗi trong quá trình xử lý')
                           callback(lstErr, null);
                       }
                       else
                       {
                            callback(null, user);
                       }
                   });
            },
            function(user, callback){
                if (user)
                {  
                    var data = {};
                    data.username = req.body.username;
                    data.name = user.name;
                    data.role = user.role.name;
                    jwt.sign(data, 'khintmam', {expiresIn: "2 days", algorithm: 'HS256'}, function(err, token){
                        if (err)
                        {
                           lstErr.push('Có lỗi trong quá trình xử lý')
                           callback(lstErr, null);
                        }
                        else {
                            callback(null,  {token: token, username: user.username, name: user.name, role: user.role.name});
                        } 
                    });
                 }
                 else
                 {
                     lstErr.push('Username hoặc password không đúng');
                     callback(lstErr, null);
                 }
            }
        ], function(err, data){
            if (err)
            {
                res.status(404).json({lstErr: lstErr}).end();
            }
            else res.send(data);
        });
    })
    return tokenRouter;
};
module.exports = routes;