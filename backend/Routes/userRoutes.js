var express = require('express');
var multer = require('multer');
var crypto = require('crypto');
var mime = require('mime');




var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
    });
  }
});


var upload = multer({ storage: storage });

var routes = function(User, Role){
   var userRouter = express.Router();
   var userController = require('../controllers/userController')(User, Role);
   console.log(userController);
   userRouter.route('/GetInfo')
    .get(userController.getInfo);
   userRouter.route('/')
    .get(userController.get);
   userRouter.route('/GetRoles')
   .get(userController.getRoles);
   userRouter.use('/:userID', function(req, res, next){
        User.findById(req.params.userID, function(err, user){
            if (err || !user){
                res.status(404).json({lstErr: ['Có lỗi trong quá trình xử lý']}).end();
            }
            else
            {
                req.user = user;
                next();
            }
        })
   });

   userRouter.route('/:userID')
    .get(userController.getDetail)
    .put(upload.single('img'), userController.put)
    .delete(userController.deleteUser);

    return userRouter;
  
};
module.exports = routes;